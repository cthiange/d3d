"""
Delft-3D Python Toolbox.
"""
import math
import os
import re
import shlex
import struct
import textwrap
import time
import warnings
from collections import namedtuple
from collections import defaultdict
from collections import OrderedDict
from datetime import datetime
from datetime import timedelta

import unittest

import numpy as np

#-------------------------------------------------------------------------------
# Files
#-------------------------------------------------------------------------------
def _write_access(f):
    def wrapped(self, *args, **kwargs):
        if self._mode not in ('r+','w'):
            raise IOError('File operation not supported.')
        return f(self, *args, **kwargs)
    return wrapped

class _File(object):
    """
    Base class.

    f = _File("dir/subdir/root.ext)

    f.path  = "dir/subdir/root.ext"
    f.path      = "dir/subdir"
    f.name      = "root.ext"
    f.name_root = "root"
    f.name_ext  = "ext"
    """
    def __init__(self, path, mode='r'):
        self._path = path # full path to file
        self._mode = mode # file access mode r,w or a
        self._dir  = None # file directory
        self._name = None # file basename + extension
        self._base = None # file basename
        self._ext  = None # file extension
        assert(mode in ('r','r+','w') )
        if path is None:
            return
        self._fileinfo()
        if mode == 'w':
            # open-close to erase existing file
            with open(path, 'w'):
                pass
        elif not os.path.exists(path):
            raise IOError('File not found.')

    @property
    def path(self):
        return self._path
    @path.setter
    def path(self, path):
        self.__init__(path)

    @property
    def abspath(self):
        return os.path.abspath(self.path)

    @property
    def dirname(self):
        return self._dir

    @property
    def filename(self):
        return self._name

    @property
    def basename(self):
        return self._base

    @property
    def fileext(self):
        return self._ext

    @property
    def bytes(self):
        """Number of bytes in file"""
        with open(self._path, 'rb') as f:
            f.seek(0,2)
            n = f.tell() / 4
        nint = int(n)
        assert(nint == n)
        return nint

    def dump(self, fileout=None):
        """
        Dump file contents to csv.

        Only available if implemented in child classes.
        If filout is None, output file will be filename.dmp.
        """
        raise NotImplementedError('This type of file cannot be dumped.')

    def _fileinfo(self):
        """
        Set file info.
        """
        self._dir  = os.path.dirname(self._path)
        self._name = os.path.basename(self._path)
        self._base, self._ext = os.path.splitext(self._name)
        if self._ext:
            # remove dot
            self._ext = self._ext[1:]

#-------------------------------------------------------------------------------
# Delwaq
#-------------------------------------------------------------------------------

# Delwaq hydrodynamic input - base classes
#-------------------------------------------------------------------------------
class _DelwaqHDFile(_File):
    """
    Baseclass for Delwaq hydrodynamic input files.
    """
    def __init__(self, path, mode, hoff=0, dfrmt='f'):
        _File.__init__(self, path, mode=mode)
        self._notim         = None  # size of time dimension (set if applicable)
        self._firdim        = None  # size of 1st dimension  (e.g. segments or exchanges)
        self._secdim        = 1     # size of 2nd dimension  (e.g. from/to, attribute nr)
        self._firdim_name   = None  # name of 1st dimension
        self._secdim_name   = None  # name of 2nd dimension
        self._header_size   = 0     # size of header
        self._dfrmt         = 'f'   # data format ('i' or 'f')

    @property
    def static(self):
        """True if no time dimension"""
        return self._firdim_name != 'time'

    @property
    def _dtype(self):
        return np.int if self._dfrmt=='i' else np.float

    def dump(self, fileout=None, sep=';'):
        """
        Write file contents to ascii.
        """
        if fileout is None:
            fileout = self.path + '.csv'
        elif os.path.abspath(fileout) == self.abspath:
            raise RuntimeError('Dumping to file to itself.')
        self._dump(fileout, sep)

    def _process_slice(self, _slice, nelems):
        """Checks slice bounds and returns start and stop"""
        if isinstance(_slice, slice):
            start = _slice.start
            stop  = _slice.stop
            dstep  = _slice.step
            if start is None: start = 0
            if stop  is None: stop  = nelems
            if dstep is not None:
                raise NotImplementedError('Stepping is not supported.')
            # check bounds
            if start < 0 or start >= nelems:
                raise IndexError('Index out of bounds.')
            if stop < 0 or stop > nelems:
                raise IndexError('Index out of bounds.')
        elif isinstance(_slice, (int, np.int8, np.int16, np.int32, np.int64) ):
            start, stop = _slice, _slice+1
            # check bounds
            if start < 0 or start >= nelems:
                raise IndexError('Index out of bounds.')
        else:
            raise IndexError()
        if stop < start:
            raise NotImplementedError('Reversing is not supported.')
        custom_slice = namedtuple("custom_slice",["start","stop"])
        return custom_slice(start, stop)

    def _scan(self):
        """
        Quick file scan during initialization to check and set file properties.
        """
        if self._mode=='w':
            self._prefill()
            return
        if self._header_size > 0:
            self._scan_header()
        # number of values in one time slice
        noval = self._firdim * self._secdim
        # bytes in one time slice
        nobyt = self.bytes - self._header_size / 4
        self._validate(noval, nobyt)


class _DelwaqHD1DimDynamic(_DelwaqHDFile):
    """
    Base class for dynamic data with one dimension.
    """
    def __init__(self, path, mode, firdim, notim):
        _DelwaqHDFile.__init__(self, path, mode)
        self._notim  = notim
        self._firdim = firdim
        self._firdim_name = 'time'

    def __repr__(self):
        if self._mode=='r':
            s = "{}('{}', {})".format(self.classname, self.path, self._firdim)
        elif self._mode=='r+':
            s = "{}('{}', {}, mode='{}')".format(
            self.classname, self.path, self._firdim, self._mode)
        else:
            s = "{}('{}', {}, mode='{}', notim={})".format(
            self.classname, self.path, self._firdim, self._mode, self._notim)
        return s

    def __str__(self):
        return "<d3d.{} path='{}', mode='{}', shape: [{} {}s, {} {}s]>".format(
            self.classname, self.path, self._mode,
            self._notim, self._firdim_name,
            self._firdim, self._secdim_name)

    def __getitem__(self, _slice):
        if not isinstance(_slice, tuple) or len(_slice) != 2:
            raise IndexError('Expected 2 dimensions (time, space).')
        # time slice
        ts = self._process_slice(_slice[0], self._notim)
        # data slice
        ds = self._process_slice(_slice[1], self._firdim)
        # init array
        shape = (ts.stop-ts.start, ds.stop-ds.start)
        arr = np.zeros(shape, dtype=self._dtype)
        # fill array
        with open(self._path, 'rb') as f:
            f.seek(self._header_size,0)
            f.seek( ts.start * (self._firdim * 4 + 4), 1)
            f.seek(4,1) # skip timestamp value
            f.seek(4*ds.start, 1) # go to data slice
            jump = (self._firdim * 4 + 4) - shape[1]*4
            for it, t in enumerate(range(ts.start, ts.stop)):
                arr[it,:] = struct.unpack('%i%s'%(shape[1], self._dfrmt),f.read(shape[1]*4))
                f.seek(jump,1)
        # cleanup array
        if shape[0]==1 and shape[1]==1:
            arr = arr[0,0]
        elif shape[0]==1:
            arr = arr[0,:]
        elif shape[1]==1:
            arr = arr[:,0]
        return arr

    @_write_access
    def __setitem__(self, _slice, values):
        if not isinstance(_slice, tuple) or len(_slice) != 2:
            raise IndexError('Expected 2 dimensions (time, space).')
        # time slice
        ts = self._process_slice(_slice[0], self._notim)
        # data slice
        ds = self._process_slice(_slice[1], self._firdim)
        # check values shape
        shape = (ts.stop-ts.start, ds.stop-ds.start)
        if isinstance(values, int) or isinstance(values, float):
            values = np.zeros(shape, dtype=self._dtype) + values
        elif not isinstance(values, np.ndarray):
            # values as sequence, convert to np array
            values = np.array(values, dtype=self._dtype)
        if len(values.shape) == 1:
            values = values.reshape( (1,values.shape[0]) )
        elif len(values.shape) > 2:
            raise IndexError('Shape of values does not match indexes.')
        if shape[0] == values.shape[0] and shape[1] == values.shape[1]:
            pass
        elif shape[0] == values.shape[1] and shape[1] == values.shape[0]:
            values = values.transpose()
        else:
            raise IndexError('Shape of values does not match indexes.')
        # write values
        with open(self._path, 'rb+') as f:
            f.seek(self._header_size,0)
            f.seek( ts.start * (self._firdim * 4 + 4), 1)
            f.seek(4,1) # skip timestamp value
            f.seek(4*ds.start, 1) # go to data slice
            jump = (self._firdim * 4 + 4) - shape[1]*4
            for it, t in enumerate(range(ts.start, ts.stop)):
                f.write(struct.pack('%i%s'%(shape[1], self._dfrmt),*values[it,:]))
                f.seek(jump, 1)

    @property
    def notim(self):
        """Number of timestamps"""
        return self._notim

    @property
    def times(self):
        """Returns list of timestamps values"""
        return [t for t in self.iter_times()]
    @times.setter
    def times(self, values):
        """Set timestamps"""
        self._write_times(values)

    def iter_times(self):
        with open(self.path, 'rb') as f:
            offset = self._firdim *4
            for i in range(self._notim):
                yield struct.unpack('i', f.read(4))[0]
                f.seek(offset, 1)

    def _dump(self, fileout, sep):
        """
        Dump file contents to ascii.
        """
        with open(fileout, 'w') as fw:
            fw.write("# CSV dump of file: '%s'\n"%self.abspath)
            fw.write("# Number of {:12} : {}\n".format('%ss'%self._firdim_name, self._notim))
            fw.write("# Number of {:12} : {}\n".format('%ss'%self._secdim_name, self._firdim))
            fw.write("# First column : {}s\n".format(self._firdim_name))
            fw.write("# Other columns: values per {}\n".format(self._secdim_name))
            frmt = "{}" + self._firdim * ("%s{:e}"%sep) + "\n"
            with open(self.path, 'rb') as fr:
                for i in range(self._notim):
                    t = struct.unpack('i', fr.read(4))[0]
                    v = np.array(struct.unpack('%i%s'%(self._firdim, self._dfrmt), fr.read(self._firdim*4)))
                    fw.write(frmt.format(t, *v))

    def _validate(self, noval, nobyt):
        if nobyt % (1+noval) == 0:
            self._notim = int(nobyt / (1 + noval))
        else:
            raise RuntimeError('Unable to detemine number of timesteps - check number of segments/exchanges')

    @_write_access
    def _prefill(self):
        """Writes null values to file"""
        with open(self.path, 'wb') as f:
            # data
            n = self._firdim + 1
            for _ in range(self._notim):
                f.write(struct.pack('%if'%n, *[0 for _ in range(n)]))

    @_write_access
    def _write_times(self, timestamps):
        """
        Writes time values to file.
        """
        if len(timestamps) != self._notim:
            raise ValueError('Number of values does not match number of times in files.')
        offset = self._firdim * 4
        with open(self.path, 'rb+') as f:
            for it, t in enumerate(timestamps):
                f.write(struct.pack('i',t))
                f.seek(offset, 1)


class _DelwaqHD1imDStatic(_DelwaqHDFile):
    """
    Base class for static data with one dimension.
    """
    def __init__(self, path, mode, firdim):
        if mode=='w' and firdim is None:
            raise ValueError('Specify number of horizontal segments in write mode.')
        _DelwaqHDFile.__init__(self, path, mode)
        self._firdim = firdim

    def __repr__(self):
        if self._mode=='r':
            s = "{}('{}')".format(self.classname, self.path)
        elif self._mode=='r+':
            s = "{}('{}', mode='{}')".format(
            self.classname, self.path, self._mode)
        else:
            s = "{}('{}', mode='{}', nosegh={})".format(
            self.classname, self.path, self._mode, self._firdim)
        return s

    def __str__(self):
        return "<d3d.{} path='{}', mode='{}', shape: [{} {}s]>".format(
            self.classname, self.path, self._mode, self._firdim, self._firdim_name)

    def __getitem__(self, _slice):
        # data slice
        ds = self._process_slice(_slice, self._firdim)
        # init array
        shape = ds.stop-ds.start
        arr = np.zeros(shape, dtype=self._dtype)
        # fill array
        with open(self._path, 'rb') as f:
            f.seek(self._header_size,0)
            f.seek(4*ds.start, 1) # go to data slice
            arr[:] = struct.unpack('%i%s'%(shape, self._dfrmt),f.read(shape*4))
        # cleanup array
        if shape==1:
            arr = arr[0]
        return arr

    @_write_access
    def __setitem__(self, _slice, values):
        # data slice
        ds = self._process_slice(_slice, self._firdim)
        # check values shape
        shape = ds.stop-ds.start
        if isinstance(values, int) or isinstance(values, float):
            values = np.zeros( shape, dtype=self._dtype) + values
        else:
            try:
                # values as numpy array
                valshape = values.shape
            except AttributeError:
                # values as sequence
                valshape = len(values)
            if hasattr(valshape,'len'):
                raise IndexError('Shape of values does not match indexes.')
            if shape != valshape:
                raise IndexError('Shape of values does not match indexes.')
        # write values
        with open(self._path, 'rb+') as f:
            f.seek(self._header_size,0)
            f.seek(4*ds.start, 1) # go to data slice
            f.write(struct.pack('%i%s'%(shape, self._dfrmt),*values))

    def _dump(self, fileout, sep):
        """
        Dump file contents to ascii.
        """
        with open(fileout, 'w') as fw:
            with open(self.path, 'rb') as fr:
                fr.seek(self._header_size, 0)
                for i in range(self._firdim):
                    fw.write("{}\n".format(struct.unpack(self._dfrmt, fr.read(4))[0]))

    def _validate(self, noval, nobyt):
        assert(nobyt == noval)


class _DelwaqHD2DimStatic(_DelwaqHDFile):
    """
    Base class for static data with two dimension.
    """
    def __init__(self, path, mode, firdim, secdim):
        if mode=='w' and firdim is None:
            raise ValueError("Specify number of exchanges in write mode")
        _DelwaqHDFile.__init__(self, path, mode)
        self._firdim = firdim
        self._secdim = secdim

    def __repr__(self):
        if self._mode=='r':
            s = "{}('{}')".format(self.classname, self.path)
        elif self._mode=='r+':
            s = "{}('{}', mode='{}')".format(
            self.classname, self.path, self._mode)
        else:
            s = "{}('{}', mode='{}', nosegh={})".format(
            self.classname, self.path, self._mode, self._firdim)
        return s

    def __str__(self):
        return "<d3d.{} path='{}', mode='{}', shape: [{} {}s, {} {}]>".format(
            self.classname, self.path, self._mode,
            self._firdim, self._firdim_name,
            self._secdim, self._secdim_name)

    def __getitem__(self, _slice):
        if not isinstance(_slice, tuple) or len(_slice) != 2:
            raise IndexError('Expected 2 dimensions.')
        # data slice 1
        ds1 = self._process_slice(_slice[0], self._firdim)
        # data slice 2
        ds2 = self._process_slice(_slice[1], self._secdim)
        # init array
        shape = (ds1.stop-ds1.start, ds2.stop-ds2.start)
        arr   = np.zeros(shape, dtype=self._dtype)
        # fill array
        with open(self._path, 'rb') as f:
            f.seek(self._header_size,0)
            f.seek(4*ds1.start*self._secdim + 4*ds2.start, 1) # go to data slice
            jump = (4*self._secdim) - shape[1]*4
            for i in range(shape[0]):
                arr[i,:] = struct.unpack('%i%s'%(shape[1], self._dfrmt),f.read(shape[1]*4))
                f.seek(jump,1)
        # cleanup array
        if shape[0]==1 and shape[1]==1:
            arr = arr[0,0]
        elif shape[0]==1:
            arr = arr[0,:]
        elif shape[1]==1:
            arr = arr[:,0]
        return arr

    @_write_access
    def __setitem__(self, _slice, values):
        if not isinstance(_slice, tuple) or len(_slice) != 2:
            raise IndexError('Expected 2 dimensions.')
        # data slice 1
        ds1 = self._process_slice(_slice[0], self._firdim)
        # data slice 2
        ds2 = self._process_slice(_slice[1], self._secdim)
        # check values shape
        shape = (ds1.stop-ds1.start, ds2.stop-ds2.start)
        if isinstance(values, int) or isinstance(values, float):
            values = np.zeros(shape, dtype=self._dtype) + values
        elif not isinstance(values, np.ndarray):
            # values as sequence, convert to np array
            values = np.array(values, dtype=self._dtype)
        if len(values.shape) == 1:
            values = values.reshape( (1,values.shape[0]) )
        elif len(values.shape) > 2:
            raise IndexError('Shape of values does not match indexes.')
        if shape[0] == values.shape[0] and shape[1] == values.shape[1]:
            pass
        elif shape[0] == values.shape[1] and shape[1] == values.shape[0]:
            values = values.transpose()
        else:
            raise IndexError('Shape of values does not match indexes.')
        # write values
        with open(self._path, 'rb+') as f:
            f.seek(self._header_size,0)
            f.seek(ds1.start * self._secdim * 4, 1)
            f.seek(ds2.start * 4, 1) # go to data slice
            jump = (4*self._secdim) - shape[1]*4
            for i in range(shape[0]):
                f.write(struct.pack('%i%s'%(shape[1], self._dfrmt),*values[i,:]))
                f.seek(jump, 1)

    def _dump(self, fileout, sep):
        """
        Dump file contents to ascii.
        """
        frmt = sep.join(['{}' for i in range(self._secdim)]) + '\n'
        with open(fileout, 'w') as fw:
            with open(self.path, 'rb') as fr:
                fr.seek(self._header_size, 0)
                for i in range(self._firdim):
                    v = struct.unpack('%i%s'%(self._secdim, self._dfrmt), fr.read(self._secdim*4))
                    fw.write(frmt.format(*v))

    def _validate(self, noval, nobyt):
        assert(nobyt == noval)


# Delwaq hydrodynamic input - hydfile
#-------------------------------------------------------------------------------
def _allow_values(values):
    """Decorator to validate values"""
    def wrap(f):
        def _wrap(self, value):
            if value not in values:
                raise ValueError("Value of '{}' must be one of {}".format(f.__name__,values))
            f(self, value)
        return _wrap
    return wrap

def _allow_types(*args):
    """Decorator to validate types"""
    def wrap(f):
        def _wrap(self, value):
            for _type in args:
                if isinstance(value, _type):
                    f(self, value)
                    break
            else:
                types = ' or '.join([t.__name__ for t in args])
                raise TypeError("'{}' must be of type {}".format(f.__name__,_type.__name__))
        return _wrap
    return wrap

class DelwaqHydFile(_File):
    """
    Interface to Delwaq hydrodynamics description file (*.hyd)
    """
    def __init__(self, path, mode='r'):
        _File.__init__(self, path, mode=mode)
        self._file_created_by    = None # optional
        self._file_creation_date = None # optional
        self._task               = None # task
        self._curvilinear        = None # geometry
        self._zlayers            = None # geometry
        self._hagg               = None # horizontal aggregation
        self._minvdiff           = None # minimum vertical diffusion used
        self._vdiff              = None # vertical diffusion
        self._hydreft            = None # hydrodynamic reference time
        self._hydstrt            = None # hydrodynamic start time
        self._hydstop            = None # hydrodynamic stop time
        self._hydstep            = None # hydrodynamic timestep
        self._conreft            = None # conversion reference time
        self._constrt            = None # conversion start time
        self._constop            = None # conversion stop time
        self._constep            = None # conversion timestep
        self._mmax               = None # grid cells first direction
        self._nmax               = None # grid cells second direction
        self._kmaxhd             = None # number hydrodynamic layers
        self._kmaxwq             = None # number water quality layers
        self._datfile            = None # hydrodynamic file
        self._dwqfile            = None # aggregation file
        self._lgafile            = None # grid indices file
        self._ccofile            = None # grid coordinates file
        self._volfile            = None # volumes file
        self._arefile            = None # areas file
        self._flofile            = None # flows file
        self._poifile            = None # pointers file
        self._lenfile            = None # lengths file
        self._salfile            = None # salinity file
        self._temfile            = None # temperature file
        self._vdffile            = None # vert diffusion file
        self._srffile            = None # surfaces file
        self._dpsfile            = None # depths file
        self._lgtfile            = None # total grid file
        self._srcfile            = None # discharges file
        self._chzfile            = None # chezy coefficients file
        self._taufile            = None # shear stresses file
        self._wlkfile            = None # walking discharges file
        self._atrfile            = None # attributes file
        self._minvdiff_upplyr    = None # minimum-vert-diffusion upper-layer
        self._minvdiff_lowlyr    = None # minimum-vert-diffusion lower-layer
        self._minvdiff_intdep    = None # minimum-vert-diffusion interfacre-depth
        self._constdsp_dir1      = None # constant-dispersion first-direction
        self._constdsp_dir2      = None # constant-dispersion second-direction
        self._constdsp_dir3      = None # constant-dispersion third-direction
        self._hdlayers           = None # hydrodynamic layers
        self._wqlayers           = None # water quality layers
        if path is not None:
            self._scan()

    @property
    def file_created_by(self):
        return self._file_created_by
    @file_created_by.setter
    def file_created_by(self, value):
        self._file_created_by = value

    @property
    def file_creation_date(self):
        return self._file_creation_date
    @file_creation_date.setter
    def file_creation_date(self, value):
        self._file_creation_date = value

    @property
    def task(self):
        return self._task
    @task.setter
    @_allow_values(['full-coupling'])
    def task(self, value):
        self._task = value

    @property
    def curvilinear(self):
        return self._curvilinear
    @curvilinear.setter
    @_allow_values([False, True])
    def curvilinear(self, value):
        self._curvilinear = value

    @property
    def zlayers(self):
        return self._zlayers
    @zlayers.setter
    @_allow_values([False, True])
    def zlayers(self, value):
        self._zlayers = value

    @property
    def hagg(self):
        return self._hagg
    @hagg.setter
    @_allow_values([False, 'active-only', True])
    def hagg(self, value):
        self._hagg = value

    @property
    def minvdiff(self):
        return self._minvdiff
    @minvdiff.setter
    @_allow_values([False, True])
    def minvdiff(self, value):
        self._minvdiff = value

    @property
    def vdiff(self):
        return self._vdiff
    @vdiff.setter
    @_allow_values(['calculated'])
    def vdiff(self, value):
        self._vdiff = value

    @property
    def hydreft(self):
        return self._hydreft
    @hydreft.setter
    @_allow_types(datetime)
    def hydreft(self, value):
        self._hydreft = value

    @property
    def hydstrt(self):
        return self._hydstrt
    @hydstrt.setter
    @_allow_types(datetime)
    def hydstrt(self, value):
        self._hydstrt = value

    @property
    def hydstop(self):
        return self._hydstop
    @hydstop.setter
    @_allow_types(datetime)
    def hydstop(self, value):
        self._hydstop = value

    @property
    def hydstep(self):
        return self._hydstep
    @hydstep.setter
    @_allow_types(timedelta)
    def hydstep(self, value):
        self._hydstep = value

    @property
    def conreft(self):
        return self._conreft
    @conreft.setter
    @_allow_types(datetime)
    def conreft(self, value):
        self._conreft = value

    @property
    def constrt(self):
        return self._constrt
    @constrt.setter
    @_allow_types(datetime)
    def constrt(self, value):
        self._constrt = value

    @property
    def constop(self):
        return self._constop
    @constop.setter
    @_allow_types(datetime)
    def constop(self, value):
        self._constop = value

    @property
    def constep(self):
        return self._constep
    @constep.setter
    @_allow_types(timedelta)
    def constep(self, value):
        self._constep = value

    @property
    def mmax(self):
        return self._mmax
    @mmax.setter
    @_allow_types(int)
    def mmax(self, value):
        self._mmax = value

    @property
    def nmax(self):
        return self._nmax
    @nmax.setter
    @_allow_types(int)
    def nmax(self, value):
        self._nmax = value

    @property
    def kmaxhd(self):
        return self._kmaxhd
    @kmaxhd.setter
    @_allow_types(int)
    def kmaxhd(self, value):
        self._hdlayers = [1.0/value for i in range(value)]
        self._kmaxhd = value

    @property
    def kmaxwq(self):
        return self._kmaxwq
    @kmaxwq.setter
    @_allow_types(int)
    def kmaxwq(self, value):
        self._wqlayers = [i+1 for i  in range(value)]
        self._kmaxwq = value

    @property
    def datfile(self):
        return self._datfile
    @datfile.setter
    @_allow_types(str)
    def datfile(self, value):
        self._datfile = value

    @property
    def dwqfile(self):
        return self._dwqfile
    @dwqfile.setter
    @_allow_types(str)
    def dwqfile(self, value):
        self._dwqfile = value

    @property
    def lgafile(self):
        return self._lgafile
    @lgafile.setter
    @_allow_types(str)
    def lgafile(self, value):
        self._lgafile = value

    @property
    def ccofile(self):
        return self._ccofile
    @ccofile.setter
    @_allow_types(str)
    def ccofile(self, value):
        self._ccofile = value

    @property
    def volfile(self):
        return self._volfile
    @volfile.setter
    @_allow_types(str)
    def volfile(self, value):
        self._volfile = value

    @property
    def arefile(self):
        return self._arefile
    @arefile.setter
    @_allow_types(str)
    def arefile(self, value):
        self._arefile = value

    @property
    def flofile(self):
        return self._flofile
    @flofile.setter
    @_allow_types(str)
    def flofile(self, value):
        self._flofile = value

    @property
    def poifile(self):
        return self._poifile
    @poifile.setter
    @_allow_types(str)
    def poifile(self, value):
        self._poifile = value

    @property
    def lenfile(self):
        return self._lenfile
    @lenfile.setter
    @_allow_types(str)
    def lenfile(self, value):
        self._lenfile = value

    @property
    def salfile(self):
        return self._salfile
    @salfile.setter
    @_allow_types(str)
    def salfile(self, value):
        self._salfile = value

    @property
    def temfile(self):
        return self._temfile
    @temfile.setter
    @_allow_types(str)
    def temfile(self, value):
        self._temfile = value

    @property
    def vdffile(self):
        return self._vdffile
    @vdffile.setter
    @_allow_types(str)
    def vdffile(self, value):
        self._vdffile = value

    @property
    def srffile(self):
        return self._srffile
    @srffile.setter
    @_allow_types(str)
    def srffile(self, value):
        self._srffile = value

    @property
    def dpsfile(self):
        return self._dpsfile
    @dpsfile.setter
    @_allow_types(str)
    def dpsfile(self, value):
        self._dpsfile = value

    @property
    def lgtfile(self):
        return self._lgtfile
    @lgtfile.setter
    @_allow_types(str)
    def lgtfile(self, value):
        self._lgtfile = value

    @property
    def srcfile(self):
        return self._srcfile
    @srcfile.setter
    @_allow_types(str)
    def srcfile(self, value):
        self._srcfile = value

    @property
    def chzfile(self):
        return self._chzfile
    @chzfile.setter
    @_allow_types(str)
    def chzfile(self, value):
        self._chzfile = value

    @property
    def taufile(self):
        return self._taufile
    @taufile.setter
    @_allow_types(str)
    def taufile(self, value):
        self._taufile = value

    @property
    def wlkfile(self):
        return self._wlkfile
    @wlkfile.setter
    @_allow_types(str)
    def wlkfile(self, value):
        self._wlkfile = value

    @property
    def atrfile(self):
        return self._atrfile
    @atrfile.setter
    @_allow_types(str)
    def atrfile(self, value):
        self._atrfile = value

    @property
    def minvdiff_upplyr(self):
        return self._minvdiff_upplyr
    @minvdiff_upplyr.setter
    @_allow_types(int, float)
    def minvdiff_upplyr(self, value):
        self._minvdiff_upplyr = value

    @property
    def minvdiff_lowlyr(self):
        return self._minvdiff_lowlyr
    @minvdiff_lowlyr.setter
    @_allow_types(int, float)
    def minvdiff_lowlyr(self, value):
        self._minvdiff_lowlyr = value

    @property
    def minvdiff_intdep(self):
        return self._minvdiff_intdep
    @minvdiff_intdep.setter
    @_allow_types(int, float)
    def minvdiff_intdep(self, value):
        self._minvdiff_intdep = value

    @property
    def constdsp_dir1(self):
        return self._constdsp_dir1
    @constdsp_dir1.setter
    @_allow_types(int, float)
    def constdsp_dir1(self, value):
        self._constdsp_dir1 = value

    @property
    def constdsp_dir2(self):
        return self._constdsp_dir2
    @constdsp_dir2.setter
    @_allow_types(int, float)
    def constdsp_dir2(self, value):
        self._constdsp_dir2 = value

    @property
    def constdsp_dir3(self):
        return self._constdsp_dir3
    @constdsp_dir3.setter
    @_allow_types(int, float)
    def constdsp_dir3(self, value):
        self._constdsp_dir3 = value

    @property
    def hdlayers(self):
        return self._hdlayers
    @hdlayers.setter
    @_allow_types(int, float, list)
    def hdlayers(self, value):
        if hasattr(value, '__len__') and len(value) == self.kmaxhd:
            self._hdlayers = value
        elif self._kmaxhd == 1:
            self._hdlayers=[value]
        else:
            raise ValueError('Number of values should match number of layers (kmaxhd)')

    @property
    def wqlayers(self):
        return self._wqlayers
    @wqlayers.setter
    @_allow_types(int, float, list)
    def wqlayers(self, value):
        if hasattr(value, '__len__') and len(value)==self.kmaxwq:
            self._wqlayers = value
        elif self._kmaxwq==1:
            self._wqlayers=[value]
        else:
            raise ValueError('Number of values should match number of layers (kmaxwq)')

    def __repr__(self):
        return "DelwaqHydFile('{}','{}')".format(self.path, self._mode)

    def __str__(self):
        return self.__repr__()

    @staticmethod
    def str2datetime(s):
        assert(len(s)==14)
        return datetime(int(s[:4]),int(s[4:6]),int(s[6:8]),int(s[8:10]),int(s[10:12]),int(s[12:]))

    @staticmethod
    def datetime2str(d):
        return "{:04}{:02}{:02}{:02}{:02}{:02}".format(d.year,d.month,d.day,d.hour,d.minute,d.second)

    @staticmethod
    def str2timedelta(s):
        assert(len(s)==14)
        assert(s[0:6]=='000000')
        dy = int(s[6:8])
        hr = int(s[8:10])
        mn = int(s[10:12])
        sc = int(s[12:])
        return timedelta(dy,hr*3600+mn*60+sc)

    @staticmethod
    def timedelta2str(d):
        return "{:04}{:02}{:02}{}".format(0,0,d.days,time.strftime('%H%M%S',time.gmtime(d.seconds)))

    def _scan(self):
        if self._mode == 'w':
            self._set_defaults()
            return
        with open(self.path, 'r') as f:
            lines = [line.strip() for line in f.readlines()]
        d = {re.findall(r'\S+', line)[0]:re.findall(r'\S+', line)[1:] for line in lines}
        k='task';                        self.task    = d[k][0]
        k='geometry';                    self.curvilinear = 'curvilinear-grid' in d[k]
        k='geometry';                    self.zlayers  = 'z-layers' in d[k]
        k='horizontal-aggregation';      self.hagg     = 'active-only' if 'automatic' in d[k] else 'yes' in d[k]
        k='minimum-vert-diffusion-used'; self.minvdiff = 'yes' in d[k]
        k='vertical-diffusion';          self.vdiff    = d[k][0]
        k='reference-time';              self.hydreft  = self.str2datetime(d[k][0][1:-1])
        k='hydrodynamic-start-time';     self.hydstrt  = self.str2datetime(d[k][0][1:-1])
        k='hydrodynamic-stop-time';      self.hydstop  = self.str2datetime(d[k][0][1:-1])
        k='hydrodynamic-timestep';       self.hydstep  = self.str2timedelta(d[k][0][1:-1])
        k='conversion-ref-time';         self.conreft  = self.str2datetime(d[k][0][1:-1])
        k='conversion-start-time';       self.constrt  = self.str2datetime(d[k][0][1:-1])
        k='conversion-stop-time';        self.constop  = self.str2datetime(d[k][0][1:-1])
        k='conversion-timestep';         self.constep  = self.str2timedelta(d[k][0][1:-1])
        k='grid-cells-first-direction';  self.mmax     = int(d[k][0])
        k='grid-cells-second-direction'; self.nmax     = int(d[k][0])
        k='number-hydrodynamic-layers';  self.kmaxhd   = int(d[k][0])
        k='number-water-quality-layers'; self.kmaxwq   = int(d[k][0])
        k='hydrodynamic-file';           self.datfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='aggregation-file';            self.dwqfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='grid-indices-file';           self.lgafile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='grid-coordinates-file';       self.ccofile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='volumes-file';                self.volfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='areas-file';                  self.arefile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='flows-file';                  self.flofile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='pointers-file';               self.poifile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='lengths-file';                self.lenfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='salinity-file';               self.salfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='temperature-file';            self.temfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='vert-diffusion-file';         self.vdffile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='surfaces-file';               self.srffile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='depths-file';                 self.dpsfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='total-grid-file';             self.lgtfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='discharges-file';             self.srcfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='chezy-coefficients-file';     self.chzfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='shear-stresses-file';         self.taufile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='walking-discharges-file';     self.wlkfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='attributes-file';             self.atrfile  = '' if d[k][0]=='none' else d[k][0][1:-1]
        k='upper-layer';                 self.minvdiff_upplyr = float(d[k][0])
        k='lower-layer';                 self.minvdiff_lowlyr = float(d[k][0])
        k='interface-depth';             self.minvdiff_intdep = float(d[k][0])
        k='first-direction';             self.constdsp_dir1   = float(d[k][0])
        k='second-direction';            self.constdsp_dir2   = float(d[k][0])
        k='third-direction';             self.constdsp_dir3   = float(d[k][0])
        i=lines.index('hydrodynamic-layers')+1
        self.hdlayers = [float(lines[i+inc]) for inc in range(self.kmaxhd)]
        i=lines.index('water-quality-layers')+1
        self.wqlayers = [float(lines[i+inc]) for inc in range(self.kmaxwq)]
        if lines.index('discharges')+1!=lines.index('end-discharges'):
            raise NotImplementedError('No support for discharges yet')

    def _set_defaults(self):
        self.task            = 'full-coupling'
        self.curvilinear     = True
        self.zlayers         = False
        self.hagg            = 'active-only'
        self.minvdiff        = False
        self.vdiff           = 'calculated'
        self.hydreft         = datetime(2000,1,1,0,0,0)
        self.hydstrt         = datetime(2000,1,1,0,0,0)
        self.hydstop         = datetime(2000,1,1,0,0,0)
        self.hydstep         = timedelta(0)
        self.conreft         = datetime(2000,1,1,0,0,0)
        self.constrt         = datetime(2000,1,1,0,0,0)
        self.constop         = datetime(2000,1,1,0,0,0)
        self.constep         = timedelta(0)
        self.mmax            = 0
        self.nmax            = 0
        self.kmaxhd          = 0
        self.kmaxwq          = 0
        self.minvdiff_upplyr = 0.0
        self.minvdiff_lowlyr = 0.0
        self.minvdiff_intdep = 0.0
        self.constdsp_dir1   = 1.0
        self.constdsp_dir2   = 1.0
        self.constdsp_dir3   = 0.0000001

    @_write_access
    def save(self):
        self._save(self.path)

    def save_as(self, filename):
        if filename == self.path:
            raise IOError('Overwriting file not allowed in read mode.')
        else:
            self._save(filename)

    def _save(self, filename):
        s = ""
        s+="{:10}{}\n".format('task',self.task)
        if self.curvilinear:
            s+="{:10}{}".format('geometry','curvilinear-grid')
        else:
            raise NotImplementedError()
        if self.zlayers:
            s+="  z-layers\n"
        else:
            s+="\n"
        if self.hagg == 'active-only':
            s+="{:29}{}\n".format('horizontal-aggregation','automatic')
        elif self.hagg:
            s+="{:29}{}\n".format('horizontal-aggregation','yes')
        else:
            s+="{:29}{}\n".format('horizontal-aggregation','no')
        if self.minvdiff:
            s+="{:29}{}\n".format('minimum-vert-diffusion-used','yes')
        else:
            s+="{:29}{}\n".format('minimum-vert-diffusion-used','no')
        s+="{:29}{}\n".format('vertical-diffusion',self.vdiff)
        s+="description\n" + 3*("'"+60*' '+"'\n") + "end-description\n"
        s+="{:25}{}\n".format('reference-time'         , "'%s'"%self.datetime2str(self.hydreft))
        s+="{:25}{}\n".format('hydrodynamic-start-time', "'%s'"%self.datetime2str(self.hydstrt))
        s+="{:25}{}\n".format('hydrodynamic-stop-time' , "'%s'"%self.datetime2str(self.hydstop))
        s+="{:25}{}\n".format('hydrodynamic-timestep'  , "'%s'"%self.timedelta2str(self.hydstep))
        s+="{:25}{}\n".format('conversion-ref-time'    , "'%s'"%self.datetime2str(self.conreft))
        s+="{:25}{}\n".format('conversion-start-time'  , "'%s'"%self.datetime2str(self.constrt))
        s+="{:25}{}\n".format('conversion-stop-time'   , "'%s'"%self.datetime2str(self.constop))
        s+="{:25}{}\n".format('conversion-timestep'    , "'%s'"%self.timedelta2str(self.constep))
        s+="{:27}{:7}\n".format('grid-cells-first-direction',self.mmax)
        s+="{:27}{:7}\n".format('grid-cells-second-direction',self.nmax)
        s+="{:27}{:7}\n".format('number-hydrodynamic-layers',self.kmaxhd)
        s+="{:27}{:7}\n".format('number-water-quality-layers',self.kmaxwq)
        s+="{:25}{}\n".format('hydrodynamic-file'       , "'%s'"%self.datfile if self.datfile else 'none')
        s+="{:25}{}\n".format('aggregation-file'        , "'%s'"%self.dwqfile if self.dwqfile else 'none')
        s+="{:25}{}\n".format('grid-indices-file'       , "'%s'"%self.lgafile if self.lgafile else 'none')
        s+="{:25}{}\n".format('grid-coordinates-file'   , "'%s'"%self.ccofile if self.ccofile else 'none')
        s+="{:25}{}\n".format('volumes-file'            , "'%s'"%self.volfile if self.volfile else 'none')
        s+="{:25}{}\n".format('areas-file'              , "'%s'"%self.arefile if self.arefile else 'none')
        s+="{:25}{}\n".format('flows-file'              , "'%s'"%self.flofile if self.flofile else 'none')
        s+="{:25}{}\n".format('pointers-file'           , "'%s'"%self.poifile if self.poifile else 'none')
        s+="{:25}{}\n".format('lengths-file'            , "'%s'"%self.lenfile if self.lenfile else 'none')
        s+="{:25}{}\n".format('salinity-file'           , "'%s'"%self.salfile if self.salfile else 'none')
        s+="{:25}{}\n".format('temperature-file'        , "'%s'"%self.temfile if self.temfile else 'none')
        s+="{:25}{}\n".format('vert-diffusion-file'     , "'%s'"%self.vdffile if self.vdffile else 'none')
        s+="{:25}{}\n".format('surfaces-file'           , "'%s'"%self.srffile if self.srffile else 'none')
        s+="{:25}{}\n".format('depths-file'             , "'%s'"%self.dpsfile if self.dpsfile else 'none')
        s+="{:25}{}\n".format('total-grid-file'         , "'%s'"%self.lgtfile if self.lgtfile else 'none')
        s+="{:25}{}\n".format('discharges-file'         , "'%s'"%self.srcfile if self.srcfile else 'none')
        s+="{:25}{}\n".format('chezy-coefficients-file' , "'%s'"%self.chzfile if self.chzfile else 'none')
        s+="{:25}{}\n".format('shear-stresses-file'     , "'%s'"%self.taufile if self.taufile else 'none')
        s+="{:25}{}\n".format('walking-discharges-file' , "'%s'"%self.wlkfile if self.wlkfile else 'none')
        s+="{:25}{}\n".format('attributes-file'         , "'%s'"%self.atrfile if self.atrfile else 'none')
        s+="{:25}\n".format('minimum-vert-diffusion')
        s+="   upper-layer    {:13.4e}\n".format(self.minvdiff_upplyr)
        s+="   lower-layer    {:13.4e}\n".format(self.minvdiff_lowlyr)
        s+="   interface-depth{:13.4e}\n".format(self.minvdiff_intdep)
        s+="end-minimum-vert-diffusion\n"
        s+="{:26}\n".format('constant-dispersion')
        s+="   first-direction {:13.4e}\n".format(self.constdsp_dir1)
        s+="   second-direction{:13.4e}\n".format(self.constdsp_dir2)
        s+="   third-direction {:13.4e}\n".format(self.constdsp_dir3)
        s+="end-constant-dispersion\n"
        s+="hydrodynamic-layers\n"
        for val in self.hdlayers:
            s+="{:15.3f}\n".format(val)
        s+="end-hydrodynamic-layers\n"
        s+="water-quality-layers   \n"
        for val in self.wqlayers:
            s+="{:13.3f}\n".format(val)
        s+="end-water-quality-layers\n"
        s+="discharges\n"
        s+="end-discharges\n"
        with open(filename, 'w') as f:
            f.write(s)


# Delwaq hydrodynamic input - grid
#-------------------------------------------------------------------------------
class DelwaqGrid(object):
    """
    Delwaq grid processing (lga & cco files).
    """
    def __init__(self, lga=None, cco=None, spherical=False, inact=False):
        """
        Specify lga and/or cco files as keyword arguments.

        Default assumes files generated with option "remove inactive cells".
        Set inact to True to deal with files generated without any aggregation.
        """
        # files
        self._cco   = None  # cco file
        self._lga   = None  # cco file
        self._inact  = inact # False if inactive cells have been removed
        # lga & cco (make sure those are equal for in both files)
        self._mmax  = None # number of grid points in m direction
        self._nmax  = None # number of grid points in n direction
        self._nlay  = None # number of layers
        # lga specific
        self._nm    = None # number of active grid points in 2D layer
        self._noq1  = None # number of exchanges in first direction (horizontal)
        self._noq2  = None # number of exchanges in second direction (horizontal)
        self._noq3  = None # number of exchanges in third direction (vertical)
        self._segs  = None # segments numbers
        # cco specific
        self._x0    = None # x coordinate of origin
        self._y0    = None # y coordinate of origin
        self._coxs  = None # corner x coordinates
        self._coys  = None # corner y coordinates
        self._alpha = None # cco attribute always zero
        self._npart = None # cco attribute always zero
        self._cart  = not spherical # True if cartesian coordinate system
        # lga derived
        self._noseg = None # total number of active segments in all layers
        self._nobnd = None # total number of boundary segments in all layers
        self._nobndh= None # number of boundary segments in 2D layer
        self._s2is  = None # segment to lga indexes lookup
        self._isag  = None # True if at least one segment is an aggregate
        # cco derived
        self._cexs  = None # center x coordinates
        self._ceys  = None # center y coordinates
        self._nocen = None # number of cell centers
        if lga is not None:
            self.lga = lga
        if cco is not None:
            self.cco = cco

    #---------------------------------------------------------------------------
    # grid properties
    #---------------------------------------------------------------------------
    @property
    def cco(self):
        return self._cco
    @cco.setter
    def cco(self, filename):
        self._cco = filename
        self._load_cco()

    @property
    def lga(self):
        return self._lga
    @lga.setter
    def lga(self, filename):
        self._lga = filename
        self._load_lga()

    @property
    def inact(self):
        return self._inact

    @property
    def mmax(self):
        return self._mmax
    @mmax.setter
    def mmax(self, value):
        self._mmax = value

    @property
    def nmax(self):
        return self._nmax
    @nmax.setter
    def nmax(self, value):
        self._nmax = value

    @property
    def nolay(self):
        return self._nlay
    @nolay.setter
    def nolay(self, value):
        self._nlay = value
        self._update_lga_attributes()

    @property
    def noq(self):
        noq = 0
        for nq in (self._noq1, self._noq2, self._noq3):
            if nq is not None:
                noq += nq
        return noq

    @property
    def noq1(self):
        return self._noq1

    @property
    def noq2(self):
        return self._noq2

    @property
    def noq3(self):
        return self._noq3

    @property
    def x0(self):
        return self._x0
    @x0.setter
    def x0(self, value):
        self._x0 = value

    @property
    def y0(self):
        return self._y0
    @y0.setter
    def y0(self, value):
        self._y0 = value

    @property
    def coxs(self):
        return self._coxs
    @coxs.setter
    def coxs(self, value):
        self._coxs = value
        self._update_cco_attributes()

    @property
    def coys(self):
        return self._coys
    @coys.setter
    def coys(self, value):
        self._coys = value
        self._update_cco_attributes()

    @property
    def segs(self):
        return self._segs
    @segs.setter
    def segs(self, segs):
        assert (len(segs) == self.mmax*self.nmax)
        self._segs = segs
        self._update_lga_attributes()

    @property
    def nosegh(self):
        return self._nm

    @property
    def noseg(self):
        return self._noseg

    @property
    def nobnd(self):
        return self._nobnd

    @property
    def nobndh(self):
        return self._nobndh

    @property
    def cartesian(self):
        return self._cart
    @cartesian.setter
    def cartesian(self, value):
        self._cart = value

    @property
    def spherical(self):
        return not self._cart
    @spherical.setter
    def spherical(self, value):
        self._cart = not value

    @property
    def aggregated(self):
        return self._isag

    @property
    def info(self):
        """
        return a description string
        """
        s = ''
        s += 'cco : %s\n'%self._cco
        s += 'lga : %s\n'%self._lga
        s += 'mmax: %i\n'%self._mmax
        s += 'nmax: %i\n'%self._nmax
        s += 'nm  : %i\n'%self._nm
        s += 'nlay: %i\n'%self._nlay
        return s

    def __repr__(self):
        frmt = "DelwaqGrid(lga='{}',cco='{}',spherical={},inact={})"
        if self._segs is None:
            noseg, mmax, nmax, nlay = '?','?','?','?'
        else:
            noseg, mmax, nmax, nlay = self._noseg, self._mmax, self._nmax, self._nlay
        return frmt.format(self._lga,self._cco,self.spherical,self._inact,noseg,mmax,nmax,nlay,noseg)

    def __str__(self):
        fmrt = "<DelwaqGrid {} {}x{}x{} noseg={}>"
        proj = 'cartesian' if self.cartesian else 'spherical'
        if self._segs is None:
            noseg, mmax, nmax, nlay = '?','?','?','?'
        else:
            noseg, mmax, nmax, nlay = self._noseg, self._mmax, self._nmax, self._nlay
        return "<DelwaqGrid {} {}x{}x{} noseg={}>".format(proj, self._mmax, self._nmax, self._nlay, self.noseg)

    #---------------------------------------------------------------------------
    # grid interface functions
    #---------------------------------------------------------------------------
    def xy2seg(self, xys):
        """
        Get 2D segment number from coordinate(s).

        *xys*   : coordinates ((x,y), ...)

        The segment corresponding to coordinates is the one whose center is closest
        to given coordinates (i.e. not necessarily a segment containing the coordinates).
        """
        # find indexes of closest centers
        cen_indexes = [self._closest_center(c[0], c[1]) for c in xys]
        # convert center indexes to lga indexes
        lga_indexes = self._centerindex_to_lgaindex(cen_indexes)
        # convert lga indexes to 2D segment numbers
        segs = [self._segs[i] for i in lga_indexes]
        return segs

    def seg2xy(self, seg, center=True):
        """
        Convert segment number to xy coordinates.

        *seg*   : segment number
        *center*: if true, return coordinates of segment center [(x, y),...] for cell in *seg*
                  if false, return coordinates of segment corners as
                  [((x0, y0), (x1, y1), (x2, y2), (x3, y3)), ...]
                    3---2
                    |   |
                    0---1
                  for each cell in *seg*

                  Boundary segments only have two points: 0--1.

                  Corner coordinates for active segments are always returned in anti-clockwise
                  order starting at the corner with the smallest index.
                  Corner coordinates for boundary segments are returned is a similar way, the
                  only difference being that the two dummy coordinates are filtered out.
        """
        seg = self.seg2d(seg)
        if seg > 0:
            return self._act2xy(seg, center=center)
        elif seg< 0:
            return self._bnd2xy(seg, center=center)

    def seg2d(self, seg):
        """
        Convert 3D segment number(s) to 2D segment number(s).

        *seg*   : segment number as int

        Segment numbers in top layer are returned unchanged.
        """
        assert(seg!=0)
        if seg > 0:
            s = seg % self._nm
            if s == 0:
                s = self._nm
        else:
            s = -(-(seg) % self.nobndh)
            if s == 0:
                s = -self._nobndh
        return s

    def seg3d(self, segs, lyrs):
        """
        Convert 2D segment number(s) to 3D segment number(s).

        *segs*  : int or sequence | segment number(s) to convert
        *lyrs*  : int or sequence | corresponding layer numbers (1=surface, n=bottom)

        If *lyrs* is a single value, it is used for all *seg* values.
        """
        if isinstance(segs, int): segs = (segs,)
        if isinstance(lyrs, int): lyrs = [lyrs for s in segs]
        assert len(segs) == len(lyrs)
        return tuple(segs[i] + (lyrs[i]-1) * self._nm for i in range(len(segs)) )

    def seg2layer(self, seg):
        """ Return layer in which seg is located (1 is top) """
        return (seg // self._nm) + 1

    def save_cco(self, filename):
        """
        Generates a cco file with name *filename*
        """
        with open(filename, 'wb') as f:
            f.write(struct.pack('i', self._mmax))
            f.write(struct.pack('i', self._nmax))
            f.write(struct.pack('f', self._x0))
            f.write(struct.pack('f', self._y0))
            f.write(struct.pack('f', 0.0))
            f.write(struct.pack('i', 0))
            f.write(struct.pack('i', self._nlay))
            f.write(struct.pack('9i',0,0,0,0,0,0,0,0,0))
            n = self.mmax * self.nmax
            f.write(struct.pack('%if'%n, *self._coxs))
            f.write(struct.pack('%if'%n, *self._coys))

    def save_lga(self, filename):
        """
        Generates an lga file with name *filename*
        """
        with open(filename, 'wb') as f:
            f.write(struct.pack('i', self._nmax))
            f.write(struct.pack('i', self._mmax))
            f.write(struct.pack('i', self._nm))
            f.write(struct.pack('i', self._nlay))
            f.write(struct.pack('i', self._noq1))
            f.write(struct.pack('i', self._noq2))
            f.write(struct.pack('i', self._noq3))
            n = self.mmax * self.nmax
            f.write(struct.pack('%ii'%n, *self._segs))

    def dump_cco(self, filename=None):
        if filename is None:
            filename = self.cco+'.csv'
        with open(filename, 'w') as f:
            f.write("x ({},{})".format(self.mmax, self.nmax))
            frmt = self.nmax * ";{}" + '\n'
            f.write(frmt.format(*[i+1 for i in range(self.nmax)]))
            for i in range(self.mmax):
                f.write("{}".format(i+1))
                f.write(frmt.format(*self._coxs[i*self.nmax:(i+1)*self.nmax]))
            f.write("\n")
            f.write("y ({},{})".format(self.mmax, self.nmax))
            frmt = self.nmax * ";{}" + '\n'
            f.write(frmt.format(*[i+1 for i in range(self.nmax)]))
            for i in range(self.mmax):
                f.write("{}".format(i+1))
                f.write(frmt.format(*self._coys[i*self.nmax:(i+1)*self.nmax]))

    def dump_lga(self, filename=None):
        if filename is None:
            filename = self.lga+'.csv'
        with open(filename, 'w') as f:
            f.write("seg ({},{})".format(self.mmax, self.nmax))
            frmt = self.nmax * ";{}" + '\n'
            f.write(frmt.format(*[i+1 for i in range(self.nmax)]))
            for i in range(self.mmax):
                f.write("{}".format(i+1))
                f.write(frmt.format(*self._segs[i*self.nmax:(i+1)*self.nmax]))

    def pointers(self, transpose=False):
        """
        Generates pointers from segment numbers.

        Returns [(ifrom, ito,ifrom-1,ito+1), ... ]
        """
        pointers1  = []
        pointers2  = []
        pointers3  = []
        pointersx = []
        # noq1 in first layer
        for m in range(self.mmax):
            for n in range(self.nmax-1):
                sfr = self._segs[m*self.nmax + n]
                sto = self._segs[m*self.nmax + n + 1]
                if sfr==0 or sto==0:
                    continue
                if sfr < 0 and sto < 0:
                    continue
                if sfr != sto:
                    # find from n-1
                    sfrn1 = 0
                    for i in range(m*self.nmax + n,0,-1):
                        seg = self.segs[i]
                        if seg == 0:
                            break
                        elif seg == sfr:
                            continue
                        else:
                            sfrn1 = seg
                            break
                    # find to n+1
                    ston1 = 0
                    for i in range(m*self.nmax+n+1,m*self.nmax+self.nmax):
                        seg = self._segs[i]
                        if seg == 0:
                            break
                        elif seg == sto:
                            continue
                        else:
                            ston1 = seg
                            break
                    pointersx.append( (sfr, sto, sfrn1, ston1) )
        # noq 1 in other layers
        pointers1.extend(pointersx)
        for ilay in range(1,self._nlay):
            incseg = self._nm     * ilay
            incbnd = self._nobndh * ilay
            for p in pointersx:
                pointers1.append( tuple(s+incseg if s > 0 else s-incbnd if s < 0 else 0 for s in p) )
        # noq2 in first layer
        pointersx = []
        for m in range(self.mmax-1):
            for n in range(self.nmax):
                sfr = self._segs[m*self.nmax + n]
                sto = self._segs[m*self.nmax + n + self.nmax]
                if sfr==0 or sto==0:
                    continue
                if sfr < 0 and sto < 0:
                    continue
                if sfr != sto:
                    # find sfr n-1
                    sfrn1 = 0
                    for i in range(m*self.nmax + n, 0, -self.nmax):
                        seg = self._segs[i]
                        if seg == 0:
                            break
                        elif seg == sfr:
                            continue
                        else:
                            sfrn1 = seg
                            break
                    # find sto n-1
                    ston1 = 0
                    for i in range((m+1)*self.nmax + n, len(self._segs), self.nmax):
                        seg = self._segs[i]
                        if seg == 0:
                            break
                        elif seg == sto:
                            continue
                        else:
                            ston1 = seg
                            break
                    pointersx.append( (sfr, sto, sfrn1, ston1) )

        # noq2 in other layers
        pointers2.extend(pointersx)
        for ilay in range(1,self._nlay):
            incseg = self._nm     * ilay
            incbnd = self._nobndh * ilay
            for p in pointersx:
                pointers2.append( tuple(s+incseg if s > 0 else s-incbnd if s < 0 else 0 for s in p) )
        if transpose:
            pointers1, pointers2 = pointers2, pointers1
        pointers = pointers1
        pointers.extend(pointers2)
        # noq 3
        for ilay in range(self._nlay-1):
            done = set()
            for seg in self._segs:
                sfr  = seg + ilay     * self._nm
                sto  = seg + (ilay+1) * self._nm
                frn1 = sfr - self._nm if ilay > 0           else 0
                ton1 = sto + self._nm if ilay < self.nolay-2 else 0
                if seg not in done and seg > 0:
                    pointers.append( (sfr, sto, frn1, ton1) )
                    done.add(seg)
        return pointers

    def surfaces(self):
        """
        Copmutes segment surfaces.

        Returns sequence of surfaces.
        """
        # init surfaces
        srfs = [0.0 for s in range(self._nm)]
        # loop over segments
        for seg in range(1, self._nm+1):
            # get segment corner coordinates
            cco_indexs = [self._lga_index_to_cco_index(ilga) for ilga in self._s2is[seg]]
            for cco_index in cco_indexs:
                x = (self._coxs[cco_index],
                     self._coxs[cco_index + self._nmax],
                     self._coxs[cco_index + self._nmax +1],
                     self._coxs[cco_index + 1])
                y = (self._coys[cco_index],
                     self._coys[cco_index + self._nmax],
                     self._coys[cco_index + self._nmax +1],
                     self._coys[cco_index + 1])
                ccs = tuple(pair for pair in zip(x,y))
                srfs[seg-1] += self._quadrilateral_area(ccs)
        return srfs

    def to_dwq(self, filename=None):
        """
        Generate *.dwq file from lga.
        """
        if filename is None:
            filename = filename = self.lga+'.dwq'
        with open(filename, 'w') as f:
            f.write("{:-12}".format(self.nmax))
            f.write("{:-12}".format(self.mmax))
            f.write("{:-12}".format(len(self._segs)))
            f.write("{:-12}".format(1))
            f.write("{:-12}\n".format(0))
            for seg in self._segs:
                f.write("{:-12}\n".format(seg))

    def convert_segment_from(self, other, seg):
        """
        Convert segment number from *other* DelwaqGrid
        """
        assert(other.nolay == self.nolay)
        layer = other.seg2layer(seg)
        xy = other.seg2xy(seg)
        myseg = self.xy2seg(xy)
        return self.seg3d(myseg, layer )



    #---------------------------------------------------------------------------
    # grid private functions
    #---------------------------------------------------------------------------
    def _load_cco(self):
        if self._cco is None:
            return
        with open(self._cco, 'rb') as f:
            # read attributes
            mmax        = struct.unpack('i', f.read(4))[0]
            nmax        = struct.unpack('i', f.read(4))[0]
            self._x0    = struct.unpack('f', f.read(4))[0]
            self._y0    = struct.unpack('f', f.read(4))[0]
            self._alpha = struct.unpack('f', f.read(4))[0] # always zero
            self._npart = struct.unpack('i', f.read(4))[0] # always zero
            nlay  = struct.unpack('i', f.read(4))[0]
            # make sure this cco matches the lga
            if self._lga is not None:
                assert self._mmax == mmax
                assert self._nmax == nmax
                assert self._nlay == nlay
            else:
                self._mmax = mmax
                self._nmax = nmax
                self._nlay = nlay
            # skip 9 zeros
            f.read(4*9)
            # read coords
            n = mmax * nmax
            self._coxs = struct.unpack('%if'%n, f.read(4 * n))
            self._coys = struct.unpack('%if'%n, f.read(4 * n))
        self._compute_centered_coordinates()

    def _load_lga(self):
        if self._lga is None:
            return
        with open(self._lga, 'rb') as f:
            # read attributes
            nmax       = struct.unpack('i', f.read(4))[0]
            mmax       = struct.unpack('i', f.read(4))[0]
            self._nm   = struct.unpack('i', f.read(4))[0]
            nlay       = struct.unpack('i', f.read(4))[0]
            self._noq1 = struct.unpack('i', f.read(4))[0]
            self._noq2 = struct.unpack('i', f.read(4))[0]
            self._noq3 = struct.unpack('i', f.read(4))[0]
            # make sure this lga matches the cco
            if self._cco is not None:
                assert self._mmax == mmax
                assert self._nmax == nmax
                assert self._nlay == nlay
            else:
                self._mmax = mmax
                self._nmax = nmax
                self._nlay = nlay
            # read segment numbers
            n = nmax * mmax
            self._segs = struct.unpack('%ii'%n, f.read(4*n))
        self._update_noseg()
        self._update_lga_attributes()

    def _update_lga_attributes(self):
        if self._segs is not None:
            self._nm = len(set([i for i in self._segs if i > 0]))
            self._update_isaggregated()
            self._update_lga_index_lookup()
            if self._nlay is not None:
                self._update_noqs()
                self._update_noseg()
                self._update_nobnd_nobndh()

    def _update_cco_attributes(self):
        if self._coxs is None or self._coys is None:
            pass
        elif len(self._coxs) == len(self._coys):
            self._update_x0y0()
            self._compute_centered_coordinates()

    def _update_isaggregated(self):
        self._isag = not len(set([i for i in self._segs if i > 0]) ) == self._nm

    def _update_nobnd_nobndh(self):
        self._nobndh = len([i for i in self._segs if i < 0])
        self._nobnd  = self.nobndh * self._nlay

    def _update_noqs(self):
        """Compute noq1, noq2 and noq3 from segment numbers"""
        # noq1 - exchanges in m direction
        noq = 0
        for m in range(self.mmax):
            for n in range(self.nmax-1):
                ifr = m*self.nmax + n
                ito = ifr+1
                if self._segs[ifr]==0 or self._segs[ito]==0:
                    continue
                if self._segs[ifr] < 0 and self._segs[ito] < 0:
                    continue
                if self._segs[ifr] != self._segs[ito]:
                    noq += 1
        self._noq1 = noq * self._nlay
        # noq2 - exchanges in n direction
        noq = 0
        for n in range(self.nmax):
            for m in range(self.mmax-1):
                ifr = m*self.nmax + n
                ito = ifr+self.nmax
                if self._segs[ifr]==0 or self._segs[ito]==0:
                    continue
                if self._segs[ifr] < 0 and self._segs[ito] < 0:
                    continue
                if self._segs[ifr] != self._segs[ito]:
                    noq += 1
        self._noq2 = noq * self._nlay
        # nm -  active segments in 2D layer
        segs = set(self._segs)
        segs.add(0)
        # noq3 - exchanges in z direction
        self._noq3 = self._nm * (self._nlay-1)

    def _update_noseg(self):
        if self._inact:
            self._noseg = len(self._segs) * self._nlay
        self._noseg = self._nm * self._nlay

    def _update_lga_index_lookup(self):
        self._s2is = defaultdict(list)
        for iseg, seg in enumerate(self._segs):
            if seg==0:
                continue
            self._s2is[seg].append(iseg)

    def _update_x0y0(self):
        self._x0 = self._coxs[0]
        self._y0 = self._coxs[0]

    def _compute_centered_coordinates(self):
        """
        Computes center coordinates from corner coordinates.
        """
        self._cexs = []
        self._ceys = []
        self._nocen = (self._mmax-2) * (self._nmax-2)
        for i in range(self._nocen):
            cor_index = self._centerindex_to_cornerindex(i)[0]
            self._cexs.append( sum((self._coxs[cor_index],
                                    self._coxs[cor_index + self._nmax],
                                    self._coxs[cor_index + self._nmax + 1],
                                    self._coxs[cor_index + 1])) / 4.0 )
            self._ceys.append( sum((self._coys[cor_index],
                                    self._coys[cor_index + self._nmax],
                                    self._coys[cor_index + self._nmax + 1],
                                    self._coys[cor_index + 1])) / 4.0 )
        assert self._nocen == len(self._cexs)
        self._cexs = tuple(self._cexs)
        self._ceys = tuple(self._ceys)

    def _centerindex_to_cornerindex(self, index):
        if isinstance(index, int):
            index = (index,)
        return tuple(i + i // (self._nmax-2) * 2 for i in index)

    def _centerindex_to_lgaindex(self, index):
        if isinstance(index, int):
            index = (index,)
        return tuple(i + i // (self._nmax-2) * 2 + self._nmax + 1 for i in index)

    def _lga_index_to_cco_index(self, index):
        return index - self._nmax - 1

    def _closest_center(self, x, y):
        """
        Returns index of center coordinates closest to given coordinates.
        """
        return self._closest(self._cexs, self._ceys, x, y)

    def _closest_corner(self, x, y):
        """
        Returns index of corner coordinates closest to given coordinates.
        """
        return self._closest(self._coxs, self._coys, x, y)

    def _closest(self, xs, ys, x, y):
        """
        Return i so that (xs[i], ys[i]) is closest to (xx, yy).
        """
        n = len(xs)
        distances = [math.sqrt((x - xs[i]) * (x - xs[i]) + (y - ys[i]) * (y - ys[i])) for i in range(n)]
        return distances.index(min(distances))

    def _distance(self, pta, ptb):
        """
        Based on http://www.movable-type.co.uk/scripts/latlong.html
        """
        d = None
        if self.spherical:
            lon1,lat1 = pta
            lon2,lat2 = ptb
            R = 6371; # Radius of the earth in km
            dLat = math.radians(lat2-lat1)
            dLon = math.radians(lon2-lon1)
            a = \
            math.sin(dLat/2) * math.sin(dLat/2) + \
            math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * \
            math.sin(dLon/2) * math.sin(dLon/2)
            c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
            d = R * c * 1000 # Distance in m
        else:
            d = math.sqrt((pta[0]-ptb[0])*(pta[0]-ptb[0]) + (pta[1]-ptb[1])*(pta[1]-ptb[1]))
        return d

    def _quadrilateral_area(self, ccs):
        """
        Returns quadrilateral area from corner coordinates.

        *ccs* : corner coordinates [(x1,y1),(x2,y2),(x3,y3),(x4,y4)]

        The area is computed as the sum of the areas of two triangles forming
        the quadrilateral. This is approximative if using spherical coordinates.
        """
        p1, p2, p3, p4 = ccs
        # compute triangle 1
        a = self._distance(p1,p2)
        b = self._distance(p2,p3)
        c = self._distance(p1,p3)
        s = (a + b + c) / 2
        srf = (s*(s-a)*(s-b)*(s-c)) ** 0.5
        # compute triangle 2
        a = self._distance(p1,p4)
        b = self._distance(p4,p3)
        c = self._distance(p1,p3)
        s = (a + b + c) / 2
        srf += (s*(s-a)*(s-b)*(s-c)) ** 0.5
        return srf

    def _act2xy(self, seg, center=True):
        """
        Equivalent of seg2xy but only for active segments (not boundaries).
        """
        assert(seg > 0)
        cco_indexs = [self._lga_index_to_cco_index(ilga) for ilga in self._s2is[seg]]
        xys = []
        for cco_index in cco_indexs:
            x = (self._coxs[cco_index],
                 self._coxs[cco_index + self._nmax],
                 self._coxs[cco_index + self._nmax +1],
                 self._coxs[cco_index + 1])
            y = (self._coys[cco_index],
                 self._coys[cco_index + self._nmax],
                 self._coys[cco_index + self._nmax +1],
                 self._coys[cco_index + 1])
            if center:
                xys.append( tuple( (sum(x)/4, sum(y)/4)) )
            else:
                xys.append( tuple(pair for pair in zip(x,y)) )
        return xys

    def _bnd2xy(self, seg, center=True):
        """
        Equivalent of seg2xy but only for boundary segments.
        """
        assert(seg < 0)
        cco_indexs = [self._lga_index_to_cco_index(ilga) for ilga in self._s2is[seg]]
        xys = []
        for cco_index in cco_indexs:
            xs = (self._coxs[cco_index],
                  self._coxs[cco_index + self._nmax],
                  self._coxs[cco_index + self._nmax +1],
                  self._coxs[cco_index + 1])
            ys = (self._coys[cco_index],
                  self._coys[cco_index + self._nmax],
                  self._coys[cco_index + self._nmax +1],
                  self._coys[cco_index + 1])
            x = [c for c in xs if c != 0.0 and round(-999.9 - c, 0) != 0]
            y = [c for c in ys if c != 0.0 and round(-999.9 - c, 0) != 0]
            assert(len(x)==2)
            if center:
                xys.append( tuple( (sum(x)/2, sum(y)/2)) )
            else:
                xys.append( tuple(pair for pair in zip(x,y)) )
        return xys


# Delwaq hydrodynamic input - static segment data
#-------------------------------------------------------------------------------
class DelwaqSrfFile(_DelwaqHD1imDStatic):
    """
    Interface to Delwaq surface files (*.srf).

    f    = DelwaqSrfFile(filepath)
    data = f[horizontal segments]
    """
    def __init__(self, path, mode='r', nosegh=None):
        self.classname = "DelwaqSrfFile"
        _DelwaqHD1imDStatic.__init__(self, path, mode, nosegh)
        self._nmax        = 0 # grid dimension in n direction
        self._mmax        = 0 # grid dimension in m direction
        self._firdim_name = 'hor.segment'
        self._header_size = 24
        self._scan()

    @property
    def mmax(self):
        return self._mmax
    @mmax.setter
    @_write_access
    def mmax(self, value):
        with open(self.path, 'rb+') as f:
            f.seek(4, 0)
            f.write(struct.pack('i', value))
        self._mmax = value

    @property
    def nmax(self):
        return self._nmax
    @nmax.setter
    @_write_access
    def nmax(self, value):
        with open(self.path, 'rb+') as f:
            f.seek(0, 0)
            f.write(struct.pack('i', value))
        self._nmax = value

    @property
    def nosegh(self):
        return self._firdim

    def _scan_header(self):
        """Scan header and set attributes"""
        with open(self.path, 'rb') as f:
            self._nmax   = struct.unpack('i', f.read(4))[0]
            self._mmax   = struct.unpack('i', f.read(4))[0]
            self._firdim = struct.unpack('i', f.read(4))[0]

    @_write_access
    def _prefill(self):
        """Writes null values to file"""
        with open(self.path, 'wb') as f:
            # header
            f.write(struct.pack('i', self._nmax))
            f.write(struct.pack('i', self._mmax))
            f.write(struct.pack('i', self._firdim))
            f.write(struct.pack('i', self._firdim))
            f.write(struct.pack('i', self._firdim))
            f.write(struct.pack('i', 0))
            # data
            for i in range(self._firdim):
                f.write(struct.pack('f', 0))


class DelwaqDpsFile(DelwaqSrfFile):
    """
    Interface to Delwaq depth files (*.dps).

    f    = DelwaqDpsFile
    data = f[horizontal segments]
    """
    def __init__(self, path, mode='r', nosegh=None):
        DelwaqSrfFile.__init__(self, path, mode=mode, nosegh=nosegh)
        self.classname = 'DelwaqDpsFile'


# Delwaq hydrodynamic input - static exchange data
#-------------------------------------------------------------------------------
class DelwaqLenFile(_DelwaqHD2DimStatic):
    """
    Interface to Delwaq length file (*.len)

    f    = DelwaqLenFile(filepath)
    data = f[exchanges,from/to]
    # length at from side of exchanges:
    data = f[exchanges,0]
    # length at to side of exchanges:
    data = f[exchanges,1]
    """
    def __init__(self, path, mode='r', noq=None):
        self.classname = 'DelwaqLenFile'
        _DelwaqHD2DimStatic.__init__(self, path, mode, noq, 2)
        self._firdim_name = 'exchange'
        self._secdim_name = 'from|to'
        self._header_size = 4
        self._scan()

    @property
    def noq(self):
        """Number of exchanges"""
        return self._firdim

    def _scan_header(self):
        with open(self.path, 'rb') as f:
            # first record is noq
            self._firdim = struct.unpack('i', f.read(4))[0]

    @_write_access
    def _prefill(self):
        """Writes null values to file"""
        with open(self.path, 'wb') as f:
            # header
            f.write(struct.pack('i', self._firdim))
            # data
            for i in range(self._firdim):
                f.write(struct.pack('2f', 0, 0))


class DelwaqPoiFile(_DelwaqHD2DimStatic):
    """
    Interface to Delwaq pointer file (*.poi) (binary version)

    f    = DelwaqPoiFile(filepath)
    data = f[exchanges,from/to]
    # id of 'from' segments
    data = f[exchanges,0]
    # id of 'to' segments
    data = f[exchanges,1]
    # id of 'from-1' segments
    data = f[exchanges,2]
    # id of 'to+1' segments
    data = f[exchanges,3]
    """
    def __init__(self, path, mode='r', noq=None):
        self.classname = 'DelwaqPoiFile'
        _DelwaqHD2DimStatic.__init__(self, path, mode, noq, 4)
        if mode != 'w':
            self._firdim  = self._get_noq_from_file_size()
        self._firdim_name = 'exchange'
        self._secdim_name = 'fr|to|frn-1|ton+1'
        self._dfrmt       = 'i'
        self._scan()

    @property
    def noq(self):
        """Number of exchanges"""
        return self._firdim

    def to_ascii(self, filename):
        """Convert pointers to ascii format"""
        assert(filename!=self.path)
        with open(self.path, 'rb') as b:
            with open(filename, 'w') as a:
                for iq in range(self.noq):
                    a.write("%10i %10i %10i %10i\n"%(struct.unpack('4i', b.read(16))))

    def _get_noq_from_file_size(self):
        """returns noq based on file size"""
        with open(self.path, 'rb') as f:
            f.seek(0,2)
            fsize = f.tell()
            assert(fsize % 4 == 0)
            nbytes = int(fsize / 4)
            assert(nbytes % 4 == 0)
            return int(nbytes / 4)

    @_write_access
    def _prefill(self):
        """Writes null values to file"""
        with open(self.path, 'wb') as f:
            # data
            for i in range(self._firdim):
                f.write(struct.pack('4f', 0, 0, 0, 0))


# Delwaq hydrodynamic input - dynamic segment data
#-------------------------------------------------------------------------------
class DelwaqVolFile(_DelwaqHD1DimDynamic):
    """
    Interface to Delwaq volumes file (*.vol)

    f    = DelwaqVolFile(filepath, noseg)
    data = f[times, segments]
    """
    def __init__(self, path, noseg, mode='r', notim=None):
        self.classname = 'DelwaqVolFile'
        if mode=='w' and notim is None:
            raise ValueError("Specify number of timesteps in write mode")
        _DelwaqHD1DimDynamic.__init__(self, path, mode, noseg, notim)
        self._secdim_name = 'segment'
        self._scan()

    @property
    def noseg(self):
        """Number of segments"""
        return self._firdim


# Delwaq hydrodynamic input - dynamic exchange data
#-------------------------------------------------------------------------------
class DelwaqAreFile(_DelwaqHD1DimDynamic):
    """
    Interface to Delwaq areas files (*.are).

    f    = DelwaqAreFile(filepath, noq)
    data = f[times, exchanges]
    """
    def __init__(self, path, noq, mode='r', notim=None):
        self.classname = 'DelwaqAreFile'
        if mode=='w' and notim is None:
            raise ValueError("Specify number of timesteps in write mode")
        _DelwaqHD1DimDynamic.__init__(self, path, mode, noq, notim)
        self._secdim_name = 'exchange'
        self._scan()

    @property
    def noq(self):
        """Number of exchanges"""
        return self._firdim


class DelwaqFloFile(DelwaqAreFile):
    """
    Interface to Delwaq flow file (*.flo)

    f    = DelwaqFloFile(filepath, noq)
    data = f[times, exchanges]
    """
    def __init__(self, path, noq, mode='r', notim=None):
        self.classname = 'DelwaqFloFile'
        DelwaqAreFile.__init__(self, path, noq, mode=mode, notim=notim)


# Delwaq - Input files
#-------------------------------------------------------------------------------
def _tokenize(content, comment=';'):
    """
    Returns list of tokens from input statements.
    """
    lex = shlex.shlex(content)
    lex.commenters = comment
    lex.whitespace_split = True
    tokens = []
    for tok in list(lex):
        # shlex does not handle these
        if tok[0] == '#' and len(tok) > 2:
            tokens.append(tok[:2])
            tokens.append(tok[2:])
        elif '*' in tok:
            multiplier, string = tok.split('*')
            for imulti in range(int(multiplier)):
                tokens.append(string)
        else:
            tokens.append(tok)
    return tokens

class DelwaqDmoFile(_File):
    """
    Read Delwaq *.dmo file.
    """
    def __init__(self, path):
        _File.__init__(self, path)
        self._d = OrderedDict() # {area_name:[segments]}
        if path is not None:
            self._scan()

    def __getitem__(self, area):
        """return segments of observation area"""
        return self._d[area]

    def __iter__(self):
        self._iter_areas = self.areas
        return self

    def __next__(self):
        if len(self._iter_areas) == 0:
            raise StopIteration
        area = self._iter_areas.pop(0)
        return area, self._d[area]

    def __repr__(self):
        return "DelwaqDmoFile('{}'')".format(self.path)

    def __str__(self):
        return "<DelwaqDmoFile path='{}', 13 areas>".format(self.path, self.count)

    @property
    def count(self):
        """number of areas"""
        return len(self._d)

    @property
    def areas(self):
        """area names"""
        return [key for key in self._d]

    def _scan(self):
        """load file"""
        with open(self.path, 'r') as f:
            tokens = _tokenize(f.read())
        count = int(tokens.pop(0))
        while(tokens):
            area = tokens.pop(0)[1:-1]
            noseg = int(tokens.pop(0))
            self._d[area] = [int(tokens.pop(0)) for _ in range(noseg)]
        assert(self.count == count)


class DelwaqParFile(_DelwaqHD2DimStatic):
    """
    Interface to Delwaq GUI parameter file (*.par)

    f    = DelwaqParFile(filepath, noseg=123)
    data = f[segments,parameters]
    """
    def __init__(self, path, mode='r', noseg=None, nopar=None):
        self.classname = 'DelwaqParFile'
        if mode == 'w' and (nopar is None or noseg is None):
            raise ValueError("Specify number of segments and parameters in write mode")
        if mode != 'w':
            if nopar is None and noseg is None:
                raise ValueError("Specify number of segments or number of parameters")
            elif path is not None:
                # check or determine missing dimension
                with open(path, 'rb') as f:
                    f.seek(0,2)
                    datasize = int(f.tell() / 4) - 1
                if noseg  is None:
                    noseg = int(datasize / nopar)
                elif nopar is None:
                    nopar = int(datasize / noseg)
                assert(noseg*nopar == datasize)
        _DelwaqHD2DimStatic.__init__(self, path, mode, noseg, nopar)
        self._firdim = noseg
        self._secdim = nopar
        self._firdim_name = 'segment'
        self._secdim_name = 'parameter'
        self._header_size = 4
        self._scan()

    @property
    def nopar(self):
        """Number of parameters"""
        return self._secdim

    @property
    def noseg(self):
        """Number of segments"""
        return self._firdim

    @_write_access
    def _prefill(self):
        """Writes null values to file"""
        with open(self.path, 'wb') as f:
            # write dummy zero
            f.write(struct.pack('i',0))
            # write dummy values
            for _ in range(self.noseg):
                f.write(struct.pack('%if'%self.nopar, *[0 for _ in range(self.nopar)]))

    def _scan_header(self):
        pass


# Delwaq - output files
#-------------------------------------------------------------------------------
class _DelwaqBinaryOutputFile(_File):
    """
    Base class for Delwaq *.his and *.map files.
    """
    def __init__(self, path, mode, notim, nosub, noloc, encod):
        if mode=='w' and None in [notim, nosub, noloc]:
            raise ValueError('Specify file dimensions in write mode')
        _File.__init__(self, path, mode=mode)
        self._comments    = None  # comment lines
        self._notim       = notim # number of timesteps
        self._nosub       = nosub # total number of substances
        self._noloc       = noloc # total number of locations
        self.encoding     = encod # text encoding
        self._subnames    = None  # substance names
        self._tblock      = None  # number of values per timestep (nosub * noseg)
        self._header_size = None  # pointer to start of data block in file
        self._reftime     = None  # reference time
        self._scu         = None  # time units
        if path is not None:
            self._scan()

    def __repr__(self):
        if self._mode=='r':
            s= frmt = "{}('{}')".format(self.classname, self.path)
        elif self._mode=='r+':
            s= frmt = "{}('{}', mode='{}')".format(self.classname, self.path, self._mode)
        else:
            s= frmt = "{}('{}', mode='{}', ({},{},{}))".format(
                self.classname, self.path, self._mode, self.notim, self.nosub, self.noloc)
        return s

    def __str__(self):
        return "<d3d.{} path='{}', mode='{}', {} times, {} substances, {} {}s>".format(
            self.classname, self.path, self._mode,
            self._notim, self._nosub, self._noloc, self._loclabel)

    def __getitem__(self, keys):
        if len(keys) != 3:
            raise IndexError('Number of dimensions does not match.')
        # substance
        isub = self._substance_key_to_index(keys[0])
        # location
        iloc = self._location_key_to_index(keys[1])
        # times
        ts = self._process_slice(keys[2], self.notim)
        # init data
        d = np.zeros( ts.stop-ts.start, dtype=np.float)
        # read data
        with open(self.path, 'rb') as f:
            f.seek(self._header_size, 0)
            f.seek(ts.start * (self._tblock + 4), 1) # skip unwanted timesteps
            f.seek(4, 1) # skip timestamp
            f.seek((iloc * (self._nosub) + isub) * 4, 1) # go to first value
            for it in range(d.shape[0]):
                d[it] = struct.unpack('f',f.read(4))[0]
                f.seek(self._tblock,1)
        if d.shape[0] == 1:
            return d[0]
        return d

    def __setitem__(self, keys, values):
        if len(keys) != 3:
            raise IndexError('Number of dimensions does not match.')
        # substance
        isub = self._substance_key_to_index(keys[0])
        # location
        iloc = self._location_key_to_index(keys[1])
        # times
        ts = self._process_slice(keys[2], self.notim)
        # values
        if isinstance(values, int) or isinstance(values, float):
            d = np.zeros( ts.stop-ts.start, dtype=np.float) + values
        else:
            d = np.array(values)
        # check data shape
        if len(d.shape) != 1 or d.shape[0] != ts.stop-ts.start:
            raise IndexError('Shape of values does not match indexes.')
        # write data
        with open(self.path, 'rb+') as f:
            f.seek(self._header_size, 0)
            f.seek(ts.start * (self._tblock + 4), 1) # skip unwanted timesteps
            f.seek(4, 1) # skip timestamp
            f.seek((iloc * (self._nosub) + isub) * 4, 1) # go to first value
            for it in range(d.shape[0]):
                f.write(struct.pack('f', d[it]))
                f.seek(self._tblock,1)

    @property
    def notim(self):
        """number of times"""
        return self._notim

    @property
    def nosub(self):
        """number of substances"""
        return self._nosub

    @property
    def times(self):
        """timestamps"""
        return list(self.iter_times())
    @times.setter
    def times(self, times):
        """write timestamps to file"""
        assert(len(times)==self.notim)
        with open(self.path, 'rb+') as f:
            f.seek(self._header_size, 0)
            for t in times:
                f.write(struct.pack('i',t))
                f.seek(self._tblock, 1)

    @property
    def dates(self):
        """timestamps"""
        dates = []
        for time in self.iter_times():
            dates.append(self._reftime + timedelta(seconds=time*self._scu))
        return dates

    @property
    def subs(self):
        """substance names"""
        return list(self._subnames)
    @subs.setter
    def subs(self, _subs):
        """write substance names to file"""
        assert(len(_subs)==self.nosub)
        with open(self.path, 'rb+') as f:
            f.seek(168, 0)
            for sub in _subs:
                f.write( ("%-20s"%sub).encode() )
        self._subnames = _subs


    @property
    def reftime(self):
        """T0 - referemce time"""
        return self._reftime

    def iter_times(self):
        """iterate over times"""
        with open(self.path, 'rb') as f:
            f.seek(self._header_size, 0)
            for i in range(self._notim):
                yield struct.unpack('i', f.read(4))[0]
                f.seek(self._tblock, 1)

    def _process_slice(self, _slice, nelems):
        """Checks slice bounds and returns start and stop"""
        if isinstance(_slice, slice):
            start = _slice.start
            stop  = _slice.stop
            step  = _slice.step
            if start is None: start = 0
            if stop  is None: stop  = nelems
            if step  is not None:
                raise NotImplementedError('Stepping is not supported.')
            # check bounds
            if start < 0 or start >= nelems:
                raise IndexError('Index out of bounds.')
            if stop < 0 or stop > nelems:
                raise IndexError('Index out of bounds.')
        elif isinstance(_slice, (int, np.int8, np.int16, np.int32, np.int64)):
            start, stop = _slice, _slice+1
            # check bounds
            if start < 0 or start >= nelems:
                raise IndexError('Index out of bounds.')
        else:
            raise IndexError()
        if stop < start:
            raise NotImplementedError('Reversing is not supported.')
        custom_slice = namedtuple("custom_slice",["start","stop"])
        return custom_slice(start, stop)

    def _scan(self):
        if self._mode=='w':
            self._tblock = self._nosub * self._noloc * 4
            self._prefill()
            return
        self._scan_header()
        with open(self.path, 'rb') as f:
            data_block_size = f.seek(0, 2) - self._header_size
        # determine number of times
        self._tblock = self._nosub * self._noloc * 4
        ntimes = data_block_size / (self._tblock + 4)
        assert int(ntimes) == ntimes
        self._notim = int(ntimes)
        # parse reftime
        if 'T0' not in self._comments[3][:23]:
            # No time string, use default
            self._reftime = datetime(2000,1,1)
            self._scu = 1
        else:
            if 'scu' in self._comments[3]:
                value , unit = re.findall(r'scu=\s*(\d+)(\S{1})',self._comments[3])[0]
                if unit == 's':
                    self._scu = int(value)
                elif unit == 'd':
                    self._scu = int(value) * 86400
                else:
                    raise NotImplementedError("Time unit '{}' not supported".format(unit))
            else:
                self._scu = 1
            m = re.match(r'T0[:=] (\d+)[\.-/]*\s*(\d+)[\.-/]*\s*(\d+)[-\s]+(\d+)[\.:\s]\s*(\d+)[\.:\s]\s*(\d+)', self._comments[3])
            if m is None:
                raise ValueError('Unknown T0 timestring format')
            yr,mo,dy,hr,mi,sc = m.group(1,2,3,4,5,6)
            self._reftime = datetime(int(yr),int(mo),int(dy),int(hr),int(mi),int(sc))

    def _substance_key_to_index(self, key):
        if isinstance(key, str):
            return self._subnames.index(key)
        elif isinstance(key, int):
            if key >= 0 and key < self._noloc:
                isub = key
                return isub
            else:
                raise IndexError('Substance index out of bounds.')
        else:
            raise NotImplementedError('Slicing not supported for substances.')


class DelwaqHisFile(_DelwaqBinaryOutputFile):
    """
    Delwaq history output file.

        >>> f = DelwaqHISFile("example.his")
        >>> print(f)
        <DelwaqHisFile 'example.his', 10 times, 3 substances, 4 locations>

        >>> data = f['DO','Obs1',:]

    Get substances:

        >>> f.subs

    Get locations:

        >>> f.locs

    Dump file to csv:

        >>> f.dump("filename.csv")
    """
    def __init__(self, filename, mode='r', notim=None, nosub=None, noloc=None, encoding='utf8'):
        self.classname = 'DelwaqHisFile'
        self._loclabel = 'location'
        self._locnames = None # location names
        self._locindxs = None # location indexes
        _DelwaqBinaryOutputFile.__init__(self, filename, mode, notim, nosub, noloc, encoding)

    @property
    def noloc(self):
        """number of locations or areas"""
        return self._noloc

    @property
    def locs(self):
        """location names"""
        return list(self._locnames)
    @locs.setter
    def locs(self, locs):
        """write location names to file"""
        assert(len(locs)==self.noloc)
        with open(self.path, 'rb+') as f:
            f.seek(168+self.nosub*20, 0)
            for iloc, loc in enumerate(locs):
                f.write(struct.pack('i',iloc+1))
                f.write( ("%-20s"%loc).encode() )
        self._locnames = locs
        self._locindxs = [i+1 for i in range(self.noloc)]


    @property
    def reftime(self):
        """T0 - referemce time"""
        return self._reftime

    def dump(self, filename, sep=';'):
        subhdr = "timestamp;time"
        lochdr = "timestamp;time"
        frmt = ""
        for loc in self._locnames:
            for sub in self._subnames:
                subhdr += sep + sub
                lochdr += sep + loc
                frmt   += sep + "{}"
        frmt += '\n'
        hdr = lochdr + '\n' + subhdr + '\n'
        n = self._nosub * self._noloc
        with open(filename, 'w') as f:
            f.write(hdr)
            with open(self.path, 'rb') as g:
                g.seek(self._header_size,0)
                for it in range(self.notim):
                    t = struct.unpack('i',g.read(4))[0]
                    f.write("{};{}".format(t, self._reftime + timedelta(seconds=t)))
                    f.write(frmt.format(*struct.unpack('%if'%n, g.read(n*4))))

    def _location_key_to_index(self, key):
        """
        Return 0 based index of location.

        Key is either the location name or an index into self._locnames.
        """
        if isinstance(key, str):
            return self._locindxs[self._locnames.index(key)] - 1
        elif isinstance(key, int):
            if key >= 0 and key < self._noloc:
                return self._locindxs[key]-1
            else:
                raise IndexError('Wrong location index.')
        raise NotImplementedError('Location index must be an integer or str. Slicing not supported')

    def _prefill(self):
        """Set defaults and write null values to file"""
        # defaults
        self._reftime = datetime(2000,1,1,0,0,0)
        self._subnames = ['Substance_%05i'%(i+1) for i in range(self.nosub)]
        self._locnames = ['Location_%05i'%(i+1) for i in range(self.noloc)]
        self._locindxs = [i+1 for i in range(self.noloc)]
        # write to file
        with open(self.path, 'wb') as f:
            f.write(b' ' * 120)
            datestr = 'T0: {}  (scu=       1s)'.format(self._reftime.strftime('%Y.%m.%d %H:%M:%S'))
            f.write(datestr.encode(self.encoding))
            f.write(struct.pack('i',self.nosub))
            f.write(struct.pack('i',self.noloc))
            for sub in self.subs:
                f.write( ("%-20s"%sub).encode(self.encoding))
            for i, loc in enumerate(self.locs):
                f.write(struct.pack('i',i+1))
                f.write( ("%-20s"%loc).encode(self.encoding))
            self._header_size = f.tell()
            for it in range(self.notim):
                f.write(struct.pack('i',it))
                for _ in range(self.nosub):
                    f.write(struct.pack('%if'%self.noloc, *[0.0 for i in range(self.noloc)]))

    def _scan_header(self):
        with open(self.path, 'rb') as f:
            self._comments    = [f.read(40).decode(encoding=self.encoding).strip() for i in range(4)]
            self._nosub       = struct.unpack('i', f.read(4))[0]
            self._noloc       = struct.unpack('i', f.read(4))[0]
            self._subnames    = [f.read(20).decode(encoding=self.encoding, errors='ignore').strip() for i in range(self._nosub)]
            self._locnames    = []
            self._locindxs    = []
            self._tblock      = 4 * self._nosub * self._noloc
            for i in range(self._noloc):
                index = struct.unpack('i', f.read(4))[0]
                name  = f.read(20).decode(encoding=self.encoding).strip()
                self._locindxs.append(index)
                self._locnames.append(name)
            self._header_size = f.tell()
        # Special treatment for PART generated his files in which all location indexes equal 1:
        if len(set(self._locindxs)) != len(self._locindxs):
            self._locindxs = [i+1 for i in range(len(self._locindxs))]
        # Load Hia file if available
        hia_file = self.path[:-4] + '.hia'
        if os.path.exists(hia_file):
            self._load_hia(hia_file)

    def _load_hia(self, hia_file):
        with open(hia_file, 'r') as f:
            lines = f.readlines()
        lines = [line.strip() for line in lines]
        lines = [line for line in lines if line]
        hia = {}
        section = None
        for line in lines:
            if '[' in line:
                section = line[1:-1]
                hia[section] = {}
            else:
                key, val = line.split('=')
                hia[section][key] = val
        assert(int(hia['DioCheck']['NumberOfLocations']) == self.noloc)
        for key, val in hia['Long Locations'].items():
            index = int(key) - 1
            self._locnames[index] = val


class DelwaqMapFile(_DelwaqBinaryOutputFile):
    """
    Delwaq map output file.
    """
    def __init__(self, path, mode='r', notim=None, nosub=None, noseg=None, encoding='utf8'):
        self.classname = 'DelwaqMapFile'
        self._loclabel = 'segment'
        _DelwaqBinaryOutputFile.__init__(self, path, mode, notim, nosub, noseg, encoding)

    @property
    def noseg(self):
        """number of segments"""
        return self._noloc

    def get(self, substance):
        """
        Returns 2D array [time, segments] for substance
        """
        isub = self._substance_key_to_index(substance)
        d = np.zeros( shape=(self.notim, self._noloc), dtype=np.float)
        with open(self.path, 'rb') as f:
            f.seek(self._header_size, 0)
            for it in range(self.notim):
                b = bytes()
                f.seek(4, 1) # skip timestamp
                for iloc in range(self._noloc):
                    f.seek(isub * 4, 1) # go to substance
                    b+= f.read(4)
                    f.seek((self.nosub-isub-1) * 4, 1) # go to next segment
                d[it,:] = struct.unpack('%if'%self._noloc,b)
        return d

    def _location_key_to_index(self, key):
        if isinstance(key, int):
            if key >= 0 and key < self._noloc:
                return key
            else:
                raise IndexError('Segment index out of bounds.')
        raise NotImplementedError('Segment index must be an integer. Slicing not supported')

    @_write_access
    def _prefill(self):
        """Set defaults and write null values to file"""
        # defaults
        self._reftime = datetime(2000,1,1,0,0,0)
        self._subnames = ['Substance_%05i'%(i+1) for i in range(self.nosub)]
        # write to file
        with open(self.path, 'wb') as f:
            f.write(b' ' * 120)
            datestr = 'T0: {}  (scu=       1s)'.format(self._reftime.strftime('%Y.%m.%d %H:%M:%S'))
            f.write(datestr.encode(self.encoding))
            f.write(struct.pack('i',self.nosub))
            f.write(struct.pack('i',self.noseg))
            for sub in self.subs:
                f.write( ("%-20s"%sub).encode(self.encoding))
            self._header_size = f.tell()
            for it in range(self.notim):
                f.write(struct.pack('i',it))
                for _ in range(self.nosub):
                    f.write(struct.pack('%if'%self.noseg, *[0 for i in range(self.noseg)]))

    def _scan_header(self):
        with open(self.path, 'rb') as f:
            self._comments    = [f.read(40).decode(self.encoding).strip() for i in range(4)]
            self._nosub       = struct.unpack('i', f.read(4))[0]
            self._noloc       = struct.unpack('i', f.read(4))[0]
            self._subnames    = [f.read(20).decode(self.encoding).strip() for i in range(self._nosub)]
            self._header_size = f.tell()


#-------------------------------------------------------------------------------
# Flow
#-------------------------------------------------------------------------------
class FlowGrid(object):
    """
    Interface to Delft3D *.grd files

    g = Grid(<filename>)
    g.xs[0:g.mmax, 0:g.nmax]
    """

    def __init__(self, filename, enc=None, dep=None):
        self.grd_file = filename
        self.enc_file = enc
        self.dep_file = dep
        self.mmax = None
        self.nmax = None
        self.xs = None # numpy array of x coordinates
        self.ys = None # numpy array of y coordinates
        self.ds = None # numpy array of depth values
        self.enc_pols = [] # list of enclosure polygons nm coords
        self.missing_value = 0.0
        self.coordinate_system = None
        self.header_lines = None
        if filename is not None:
            self._load()

    def __str__(self):
        return "<Grid('{}') {} x {}>".format(self.grd_file.split(os.sep)[-1],self.mmax, self.nmax)

    def _load(self):
        with open(self.grd_file, 'r') as f:
            lines = f.readlines()
        # discard and keep comment lines
        self.header_lines = []
        while lines[0][0] == '*':
            self.header_lines.append( lines.pop(0) )
        # parse coordinate system
        r = re.findall(r'Coordinate System\s*=\s*(\S+)', lines.pop(0))
        self.coordinate_system = r[0]
        # parse missing value
        if 'Missing Value' in lines[0]:
            r = re.findall(r'Missing Value\s*=\s+([-\d.E+]+)', lines.pop(0))
            self.missing_value = np.float(r[0])
        # parse m and n max
        m,n = re.findall(r'(\d+)\s+(\d+)', lines.pop(0))[0]
        self.mmax = int(m)
        self.nmax = int(n)
        # skip 0 0 0
        lines.pop(0)
        # parse x's
        lines = ''.join(lines)
        pttrn = r'ETA=\s+(\d+)\s+((?:[-\d.E+]+\s+){%s})'%self.mmax
        pttrn = r'ETA=\s+(\d+)\s+((?:[-\d.eE+]+\s+){%s})'%self.mmax
        etas = re.findall(pttrn, lines)
        self.xs = np.zeros((self.mmax, self.nmax), dtype=np.float)
        for eta in etas[0:self.nmax]:
            n = int(eta[0])
            vals = [float(v) for v in re.findall(r'[-\d.eE+]+', eta[1])]
            self.xs[:,n-1] = vals
        self.ys = np.zeros((self.mmax, self.nmax), dtype=np.float)
        for eta in etas[self.nmax:]:
            n = int(eta[0])
            vals = [float(v) for v in re.findall(r'[-\d.eE+]+', eta[1])]
            self.ys[:,n-1] = vals
        # enc file
        if self.enc_file is not None:
            with open(self.enc_file, 'r') as f:
                self._parse_enc(f.read())
        # dep file
        if self.dep_file is not None:
            with open(self.dep_file, 'r') as f:
                content = f.read()
            pttrn = r'\s*(?:[-\d.eE+]+\s*)'
            vals = [float(v) for v in re.findall(pttrn, content)]
            self.ds = np.array(vals).reshape((self.nmax+1, self.mmax+1))
            self.ds = self.ds[0:self.nmax, 0:self.mmax]
            self.ds = self.ds.transpose()

    def _parse_enc(self, buff):
        if 'COORdinates' in buff:
            pttrn = r'\s*[(]\s*(\d+)\s*[,]\s*(\d+)\s*[)]\s*'
            pairs = [(int(x), int(y)) for x,y in re.findall(pttrn, buff)]
        else:
            pttrn = r'\s*[(]*\s*(\d+)\s*[,]*\s*(\d+).*'
            pairs = [(int(x), int(y)) for x,y in re.findall(pttrn, buff)]
        pols = []
        pol = []
        for pair in pairs:
            if pair in pol:
                pol.append(pair)
                pols.append(pol)
                pol = []
            else:
                pol.append(pair)
        self.enc_pols = pols

    def save_grd(self, filename):
        with open(filename, 'w') as f:
            for line in self.header_lines:
                f.write(line)
            f.write("Coordinate System=%s\n"%self.coordinate_system)
            f.write("Missing Value=%f\n"%self.missing_value)
            f.write(" %8i%8i\n"%(self.mmax, self.nmax))
            f.write("       0       0       0\n")

            # write xs
            for n in range(self.nmax):
                f.write(" ETA=%5i"%(n+1))
                vals = [v for v in self.xs[:,n]]
                first = True
                while(len(vals) >=5):
                    if first:
                        first = False
                    else:
                        f.write("          ")
                    f.write(" %20.18e"%vals.pop(0))
                    f.write(" %20.18e"%vals.pop(0))
                    f.write(" %20.18e"%vals.pop(0))
                    f.write(" %20.18e"%vals.pop(0))
                    f.write(" %20.18e\n"%vals.pop(0))
                f.write("          ")
                while(vals):
                    f.write(" %20.18e"%vals.pop(0))
                f.write("\n")

            # write ys
            for n in range(self.nmax):
                f.write(" ETA=%5i"%(n+1))
                vals = [v for v in self.ys[:,n]]
                first = True
                while(len(vals) >=5):
                    if first:
                        first = False
                    else:
                        f.write("          ")
                    f.write(" %20.18e"%vals.pop(0))
                    f.write(" %20.18e"%vals.pop(0))
                    f.write(" %20.18e"%vals.pop(0))
                    f.write(" %20.18e"%vals.pop(0))
                    f.write(" %20.18e\n"%vals.pop(0))
                f.write("          ")
                while(vals):
                    f.write(" %20.18e"%vals.pop(0))
                f.write("\n")
        self.grd_file = filename

    def save_dep(self, filename, ncols=5):
        ds = self.ds.copy()
        ds = np.insert(ds, self.mmax, values=-999.0, axis=0)
        ds = np.insert(ds, self.nmax, values=-999.0, axis=1)
        with open(filename, 'w') as f:
            for n in range(self.nmax+1):
                vals = [v for v in ds[:,n]]
                while(len(vals) >= ncols):
                    for _ in range(ncols):
                        f.write(" %e"%vals.pop(0))
                    f.write('\n')
                while(vals):
                    f.write(" %e"%vals.pop(0))
                f.write("\n")
        self.dep_file = filename

    def flip(self):
        """Flip n and m dimmensions"""
        self.mmax, self.nmax = self.nmax, self.mmax
        self.xs = np.transpose(self.xs)
        self.ys = np.transpose(self.ys)

    def hypso(self, nintervals=10, mma='mean'):
        """
        Compute hypsometric curve from grid and depth.

        >>> g = FlowGrid(grid, depth)
        >>> h = g.hypso()
        >>> depths = h[:,0]
        >>> areas  = h[:,1]
        """
        # choose arae function based on coordinate system
        if  self.coordinate_system == 'Cartesian':
            area_function = rectangle_area
        elif self.coordinate_system == 'Spherical':
            area_function = wgs_rectangle_area
        else:
            raise ValueError("Unknown coordinate system '{}'.".format(self.coordinate_system))
        areas  = []
        depths = []
        for m in range(self.mmax-1):
            for n in range(self.nmax-1):
                # get coordinates of current cell
                pnt_a = (self.xs[m  ,n  ], self.ys[m  ,n  ], self.ds[m  ,n  ])
                pnt_b = (self.xs[m  ,n+1], self.ys[m  ,n+1], self.ds[m  ,n+1])
                pnt_c = (self.xs[m+1,n+1], self.ys[m+1,n+1], self.ds[m+1,n+1])
                pnt_d = (self.xs[m+1,n  ], self.ys[m+1,n  ], self.ds[m+1,n  ])
                points = [pnt_a, pnt_b, pnt_c, pnt_d]
                # check if current cell is active
                active = [p[0] != self.missing_value for p in points]
                if not all(active): continue
                # compute are of current cell
                area = area_function(*points)
                # compute average depth of current cell
                if mma == 'min':
                    depth = min([p[2] for p in points])
                elif mma == 'max':
                    depth = max([p[2] for p in points])
                else:
                    depth = sum([p[2] for p in points]) / 4.0
                # store results
                areas.append(area)
                depths.append(depth)
        # compute depth min and max
        areas  = np.array(areas)
        depths = np.array(depths)
        mindep = np.min(depths)
        maxdep = np.max(depths)
        # compute cumulative area from bottom to top
        d = np.zeros( (nintervals,2), dtype=np.float)
        for i in range(nintervals):
            d[i,0] = mindep + (maxdep - mindep) * i / nintervals
            d[i,1] = np.sum(areas[depths <= d[i,0]])
        return d

    def enc2cc(self):
        """
        Return enclosure polygons in actual grid coordinates (instead of mn).
        """
        ind_pols = []
        for i, enc_pol in enumerate(self.enc_pols):
            clockwise = self._polygon_is_clockwise(enc_pol)
            # We assume there is only one external enclosure
            # so only the first one is considered as such.
            # All following enclosures are interpreted as internal
            # ones.
            # This will break for grids with non-continuous parts...
            if i == 0:
                left = not clockwise
            else:
                left = clockwise
            enc_pol_without_diagonals = self._expand_diagonals(enc_pol, left)
            ind_pols.append(self._enc2cc(enc_pol_without_diagonals, left))
        # Interpolate indices between polygon support points
        ind_pols = [self._expand_indices(ind_pol) for ind_pol in ind_pols]
        # Convert indices to coordinates
        pols = [ [(self.xs[m,n], self.ys[m,n]) for m,n in ind_pol] for ind_pol in ind_pols]
        return pols

    def _enc2cc(self, enc_pol, left):
        ind_pol = []
        # Discard last point, identical to first point
        enc_pol = enc_pol[:-1]
        # Loop over corners
        for i, cur in enumerate(enc_pol):
            if i == 0:
                # First point
                prv = enc_pol[-1]
                nxt = enc_pol[i+1]
            elif i == len(enc_pol)-1:
                # Last point
                prv = enc_pol[i-1]
                nxt = enc_pol[0]
            else:
                # Intermediate points
                prv = enc_pol[i-1]
                nxt = enc_pol[i+1]
            m,n = self._enc2ind(prv, cur, nxt, left)
            ind_pol.append( (m,n) )
        # Close polygon
        ind_pol.append(ind_pol[0])
        # Convert from 1-based to 0-based indexing
        ind_pol = [ (p[0]-1, p[1]-1) for p in ind_pol ]
        return ind_pol

    @staticmethod
    def _polygon_is_clockwise(pol):
        """
        Return True if polygon points in clockwise order.

        pol: list of polygon coordinate tuples with last==first
        """
        a = 0
        for p,q in zip(pol[:-1],pol[1:]):
            a += (q[0]-p[0])*(q[1]+p[1])
        return a > 0

    @staticmethod
    def _enc2ind(prv, cur, nxt, left):
        """
        Convert 1-based m,n enclosure indices to 1-based grid indices.

        prv: previous point
        cur: current point
        nxt: next point
        left: True if left oriented enclosure (keeps points left of polygon)
        """
        m,n = cur
        # Coordinate component differences
        dm1 = cur[0]-prv[0]
        dm2 = nxt[0]-cur[0]
        dn1 = cur[1]-prv[1]
        dn2 = nxt[1]-cur[1]
        # Normalize to -1/0/1
        sm1 = dm1 if dm1==0 else abs(dm1)/dm1
        sm2 = dm2 if dm2==0 else abs(dm2)/dm2
        sn1 = dn1 if dn1==0 else abs(dn1)/dn1
        sn2 = dn2 if dn2==0 else abs(dn2)/dn2
        # Combine in single vector
        v = (sm1+sm2, sn1+sn2)
        # Convert enc coordinates
        if v == (-1,1):
            # NW
            if left:
                m-=1
                n-=1
            else:
                pass
        elif v == (1,1):
            # NE
            if left:
                m-=1
            else:
                n-=1
        elif v == (1,-1):
            # SE
            if left:
                pass
            else:
                m-=1
                n-=1
        elif v == (-1,-1):
            # SW
            if left:
                n-=1
            else:
                m-=1
        elif v == (0,2):
            # NN
            if left:
                m-=1
                n-=1
            else:
                n-=1
        elif v == (0,-2):
            # SS
            if left:
                pass
            else:
                m-=1
        elif v == (2,0):
            # EE
            if left:
                m-=1
            else:
                m-=1
                n-=1
        elif v == (-2,0):
            if left:
                n-=1
            else:
                pass
        else:
            raise NotImplementedError('Enclosure shape not implemented. {} {}{}{}'.format(v, prv, cur, nxt))
        return (m, n)

    @staticmethod
    def _expand_indices(ind_pol):
        indices = []
        for (cur_m, cur_n), (nxt_m, nxt_n) in zip(ind_pol[:-1], ind_pol[1:]):
            dm = nxt_m - cur_m
            dn = nxt_n - cur_n
            if dm == 0 and dn == 0:
                # ignore duplicate
                continue
            if dm != 0 and dn != 0:
                raise ValueError('Indices should not contain diagonal increments. ({},{})'.format((cur_m, cur_n), (nxt_m, nxt_n)))
            if cur_m == nxt_m:
                step, inc = 1, 0
                if cur_n > nxt_n:
                    step, inc = -1, 0
                for _n in range(cur_n, nxt_n+inc, step):
                    indices.append( (cur_m,_n) )
            if cur_n == nxt_n:
                step, inc = 1, 0
                if cur_m > nxt_m:
                    step, inc = -1, 0
                for _m in range(cur_m, nxt_m+inc, step):
                    indices.append( (_m,cur_n) )
        indices.append(indices[0])
        return indices

    @staticmethod
    def _expand_diagonals(enc_pol, left):
        """
        Add corner points to remove diagonals from enclosure
        """
        # Interpolate diagonals longer than 1
        interpolated = [enc_pol[0]]
        for cur in enc_pol[1:]:
            prv = interpolated[-1]
            dm = cur[0]-prv[0]
            dn = cur[1]-prv[1]
            if abs(dm) == abs(dn):
                m,n = prv
                minc = dm//abs(dm)
                ninc = dn//abs(dn)
                for i in range(abs(dm)-1):
                    _m = int(m+minc*(i+1))
                    _n = int(n+ninc*(i+1))
                    interpolated.append((_m,_n))
            interpolated.append(cur)
        # Add corners
        expanded = [interpolated[0]]
        for cur in interpolated[1:]:
            prv = expanded[-1]
            dm = cur[0]-prv[0]
            dn = cur[1]-prv[1]
            # vertical and horizontal
            if dm==0 or dn==0:
                expanded.append(cur)
                continue
            m, n = cur
            if dm==1 and dn==1:
                if left:
                    n-=1
                else:
                    m-=1
            elif dm==1 and dn==-1:
                if left:
                    m-=1
                else:
                    n+=1
            elif dm==-1 and dn==-1:
                if left:
                    n+=1
                else:
                    m+=1
            elif dm==-1 and dn==1:
                if left:
                    m+=1
                else:
                    n-=1
            expanded.append((m,n))
            expanded.append(cur)
        return expanded



#-------------------------------------------------------------------------------
# Coordinates
#-------------------------------------------------------------------------------
def rd2wgs(x, y):
    """
    Convert RD New coordinates to WGS1984.

    Based on rd2wgs php code from http://www.god-object.com/2009/10/23/
        convert-rijksdriehoekscordinaten-to-latitudelongitude/
    """
    dX = (x - 155000) * math.pow(10, - 5)
    dY = (y - 463000) * math.pow(10, - 5)
    SomN = (3235.65389 * dY) + \
        (- 32.58297 * math.pow(dX, 2)) + \
        (- 0.2475   * math.pow(dY, 2)) + \
        (- 0.84978  * math.pow(dX, 2) * dY) + \
        (- 0.0655   * math.pow(dY, 3)) + \
        (- 0.01709  * math.pow(dX, 2) * math.pow(dY, 2)) + \
        (- 0.00738 * dX) + \
        (0.0053 * math.pow(dX, 4)) + \
        (- 0.00039 * pow(dX, 2) * math.pow(dY, 3)) + \
        (0.00033 * math.pow(dX, 4) * dY) + \
        (- 0.00012 * dX * dY)
    SomE = (5260.52916 * dX) + \
        (105.94684 * dX * dY) + \
        (2.45656 * dX * math.pow(dY, 2)) + \
        (- 0.81885 * math.pow(dX, 3)) + \
        (0.05594 * dX * math.pow(dY, 3)) + \
        (- 0.05607 * math.pow(dX, 3) * dY) + \
        (0.01199 * dY) + \
        (- 0.00256 * math.pow(dX, 3) * math.pow(dY, 2)) + \
        (0.00128 * dX * math.pow(dY, 4)) + \
        (0.00022 * math.pow(dY, 2)) + \
        (- 0.00022 * math.pow(dX, 2)) + \
        (0.00026 * math.pow(dX, 5));
    lat = 52.15517 + (SomN / 3600)
    lon = 5.387206 + (SomE / 3600)
    return (lon, lat)

def wgs2rd(x, y):
    """
    Convert WGS1984 coordinates to RD New.
    """
    raise NotImplementedError()

def distance(point_a, point_b):
    """
    Return distance between *point_a* and *point_b*.
    """
    xa, ya = point_a[0:2]
    xb, yb = point_b[0:2]
    return math.sqrt((xb-xa)**2 + (yb-ya)**2)

def rectangle_area(point_a, point_b, point_c, point_d):
    ab = distance(point_a, point_b)
    ad = distance(point_a, point_d)
    return ab * ad

def wgs_distance(point_a, point_b):
    """ Returns distance between wgs *point_a* and *point_b* in meters. """
    lon1, lat1 = point_a[0:2]
    lon2, lat2 = point_b[0:2]
    R = 6371 # Radius of the earth in km
    dLat = deg2rad(lat2-lat1)
    dLon = deg2rad(lon2-lon1)
    a = math.sin(dLat/2) * math.sin(dLat/2) + \
        math.cos(deg2rad(lat1)) * math.cos(deg2rad(lat2)) * \
        math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = R * c * 1000; # Distance in m
    return d

def wgs_rectangle_area(point_a, point_b, point_c, point_d):
    """
    Returns area of rectangle delimited by opposite wgs points
    *point_a* and *point_c* in meters.
    """
    ab = wgs_distance(point_a, point_b)
    ad = wgs_distance(point_a, point_d)
    return ab * ad

def deg2rad(deg):
    return deg * (math.pi/180)


################################################################################
#                                                                              #
#                                UNIT TESTS                                    #
#                                                                              #
################################################################################

class TestDelwaqGridWithHorizontalAggregation(unittest.TestCase):

    def setUp(self):
        self.g = DelwaqGrid(lga='data\\hagg\\com-small_grid.lga', cco='data\\hagg\\com-small_grid.cco')
        self.dwq_file = 'data\\hagg\\small_agg.dwq'

    def tearDown(self):
        pass

    def test_update_lga_attributes_sets_noqs_and_nm(self):
        self.g._noq1 = None
        self.g._noq2 = None
        self.g._noq3 = None
        self.g._nm   = None
        self.g._update_lga_attributes()
        self.assertEqual(self.g._noq1, 8*3)
        self.assertEqual(self.g._noq2, 7*3)
        self.assertEqual(self.g._noq3, 12*2)
        self.assertEqual(self.g._nm, 12)

    def test_pointers_returns_pointers(self):
        exp_ptrs = [
            (1,2,0,0),
            (3,2,0,4),
            (2,4,3,0),
            (6,7,0,0),
            (9,7,0,0),
            (8,10,0,11),
            (10,11,8,7),
            (11,7,10,0),
            (13,14,0,0),
            (15,14,0,16),
            (14,16,15,0),
            (18,19,0,0),
            (21,19,0,0),
            (20,22,0,23),
            (22,23,20,19),
            (23,19,22,0),
            (25,26,0,0),
            (27,26,0,28),
            (26,28,27,0),
            (30,31,0,0),
            (33,31,0,0),
            (32,34,0,35),
            (34,35,32,31),
            (35,31,34,0),
            (1,3,0,5),
            (3,5,1,8),
            (4,6,0,9),
            (5,8,3,0),
            (6,9,4,11),
            (9,11,6,12),
            (11,12,9,0),
            (13,15,0,17),
            (15,17,13,20),
            (16,18,0,21),
            (17,20,15,0),
            (18,21,16,23),
            (21,23,18,24),
            (23,24,21,0),
            (25,27,0,29),
            (27,29,25,32),
            (28,30,0,33),
            (29,32,27,0),
            (30,33,28,35),
            (33,35,30,36),
            (35,36,33,0),
            (1,13,0,25),
            (2,14,0,26),
            (3,15,0,27),
            (4,16,0,28),
            (5,17,0,29),
            (6,18,0,30),
            (7,19,0,31),
            (8,20,0,32),
            (9,21,0,33),
            (10,22,0,34),
            (11,23,0,35),
            (12,24,0,36),
            (13,25,1,0),
            (14,26,2,0),
            (15,27,3,0),
            (16,28,4,0),
            (17,29,5,0),
            (18,30,6,0),
            (19,31,7,0),
            (20,32,8,0),
            (21,33,9,0),
            (22,34,10,0),
            (23,35,11,0),
            (24,36,12,0)]
        com_ptrs = self.g.pointers()
        self.assertSequenceEqual( com_ptrs, exp_ptrs)

    def test_pointers_transpose_returns_transposed_pointers(self):
        exp_ptrs = [
            (1,3,0,5),
            (3,5,1,8),
            (4,6,0,9),
            (5,8,3,0),
            (6,9,4,11),
            (9,11,6,12),
            (11,12,9,0),
            (13,15,0,17),
            (15,17,13,20),
            (16,18,0,21),
            (17,20,15,0),
            (18,21,16,23),
            (21,23,18,24),
            (23,24,21,0),
            (25,27,0,29),
            (27,29,25,32),
            (28,30,0,33),
            (29,32,27,0),
            (30,33,28,35),
            (33,35,30,36),
            (35,36,33,0),
            (1,2,0,0),
            (3,2,0,4),
            (2,4,3,0),
            (6,7,0,0),
            (9,7,0,0),
            (8,10,0,11),
            (10,11,8,7),
            (11,7,10,0),
            (13,14,0,0),
            (15,14,0,16),
            (14,16,15,0),
            (18,19,0,0),
            (21,19,0,0),
            (20,22,0,23),
            (22,23,20,19),
            (23,19,22,0),
            (25,26,0,0),
            (27,26,0,28),
            (26,28,27,0),
            (30,31,0,0),
            (33,31,0,0),
            (32,34,0,35),
            (34,35,32,31),
            (35,31,34,0),
            (1,13,0,25),
            (2,14,0,26),
            (3,15,0,27),
            (4,16,0,28),
            (5,17,0,29),
            (6,18,0,30),
            (7,19,0,31),
            (8,20,0,32),
            (9,21,0,33),
            (10,22,0,34),
            (11,23,0,35),
            (12,24,0,36),
            (13,25,1,0),
            (14,26,2,0),
            (15,27,3,0),
            (16,28,4,0),
            (17,29,5,0),
            (18,30,6,0),
            (19,31,7,0),
            (20,32,8,0),
            (21,33,9,0),
            (22,34,10,0),
            (23,35,11,0),
            (24,36,12,0)]
        com_ptrs = self.g.pointers(transpose=True)
        self.assertSequenceEqual( com_ptrs, exp_ptrs)

    def test_surfaces_returns_surfaces(self):
        exp_surfs = [
            974864.9375,
            3611969.5,
            969419.75,
            831560.5625,
            967549.3125,
            829956.125,
            2359551.5,
            2872202.5,
            831435.875,
            882153.375,
            835983.4375,
            843549.4375]
        com_surfs = self.g.surfaces()
        self.assertEqual(len(com_surfs), len(exp_surfs))
        self.assertTrue(0.0 not in com_surfs)
        # self.assertSequenceEqual(com_surfs, exp_surfs)

    def test_noseg_returns_number_of_segments(self):
        self.assertEqual(12*3, self.g.noseg)

    def test_seg2d_returns_segment_number_in_first_layer(self):
        self.assertEqual( 1, self.g.seg2d( 1)) # first seg in first layer
        self.assertEqual(12, self.g.seg2d(12)) # last seg in first layer
        self.assertEqual( 1, self.g.seg2d(13)) # first seg in second layer
        self.assertEqual(12, self.g.seg2d(24)) # last seg in second layer

    def test_to_dwq_generates_dwq_file(self):
        with open(self.dwq_file, 'r') as f:
            exp = f.read()
        test_file = "_test.dwq"
        self.g.to_dwq(test_file)
        with open(test_file, 'r') as f:
            com = f.read()
        os.remove(test_file)
        self.assertEqual(com, exp)


class TestDelwaqGridWithBoundaries(unittest.TestCase):

    def setUp(self):
        self.g = DelwaqGrid(lga='data\\bound\\com-small_grid.lga', cco='data\\bound\\com-small_grid.cco')

    def tearDown(self):
        pass

    def test_seg2xy_returns_coordinares_for_boundary_segments(self):
        self.assertSequenceEqual(self.g.seg2xy(-1,center=False),
            [((1004.6166381835938, 1584.683349609375), (1004.6166381835938, 2463.949951171875))])
        self.assertSequenceEqual(self.g.seg2xy(-2,center=False),
            [((2755.083251953125, 5150.14990234375), (3630.316650390625, 5174.35009765625))])
        self.assertSequenceEqual(self.g.seg2xy(-6,center=False),
            [((6429.4501953125, 1867.0167236328125), (6167.283203125, 2705.949951171875))])

    def test_pointers_returns_pointers(self):
        exp_ptrs = [
            (1,2,0,3),
            (2,3,1,0),
            (4,5,0,6),
            (5,6,4,7),
            (6,7,5,0),
            (9,10,0,-2),
            (10,-2,9,0),
            (12,13,0,-3),
            (13,-3,12,0),
            (14,15,0,16),
            (15,16,14,17),
            (16,17,15,18),
            (17,18,16,-4),
            (18,-4,17,0),
            (20,21,0,22),
            (21,22,20,0),
            (23,24,0,25),
            (24,25,23,26),
            (25,26,24,0),
            (28,29,0,-8),
            (29,-8,28,0),
            (31,32,0,-9),
            (32,-9,31,0),
            (33,34,0,35),
            (34,35,33,36),
            (35,36,34,37),
            (36,37,35,-10),
            (37,-10,36,0),
            (-1,2,0,5),
            (1,4,0,8),
            (2,5,-1,0),
            (3,6,0,0),
            (4,8,1,11),
            (7,9,0,12),
            (8,11,4,14),
            (9,12,7,17),
            (10,13,0,18),
            (11,14,8,-5),
            (12,17,9,19),
            (13,18,10,0),
            (14,-5,11,0),
            (15,-6,0,0),
            (17,19,12,0),
            (-7,21,0,24),
            (20,23,0,27),
            (21,24,-7,0),
            (22,25,0,0),
            (23,27,20,30),
            (26,28,0,31),
            (27,30,23,33),
            (28,31,26,36),
            (29,32,0,37),
            (30,33,27,-11),
            (31,36,28,38),
            (32,37,29,0),
            (33,-11,30,0),
            (34,-12,0,0),
            (36,38,31,0),
            (1,20,0,0),
            (2,21,0,0),
            (3,22,0,0),
            (4,23,0,0),
            (5,24,0,0),
            (6,25,0,0),
            (7,26,0,0),
            (8,27,0,0),
            (9,28,0,0),
            (10,29,0,0),
            (11,30,0,0),
            (12,31,0,0),
            (13,32,0,0),
            (14,33,0,0),
            (15,34,0,0),
            (16,35,0,0),
            (17,36,0,0),
            (18,37,0,0),
            (19,38,0,0)]
        com_ptrs = self.g.pointers()
        self.assertSequenceEqual( com_ptrs, exp_ptrs)


class TestDelwaqGridGeometryCalculations(unittest.TestCase):

    def setUp(self):
        self.g = DelwaqGrid()

    def test_distance_cartesian(self):
        self.g.cartesion = True
        pta = (0,0)
        ptb = (1,0)
        self.assertEqual(self.g._distance(pta, ptb), 1.0)

    def test_spherical(self):
        self.g.spherical = True
        pta = (4.297841,51.778473)
        ptb = (4.280375,51.776349)
        self.assertEqual(self.g._distance(pta, ptb), 1224.6216923421748)

    def test_quadrilateral_area_cartesian(self):
        self.g.cartesion = True
        ccs = [(0,0),(1,0),(1,2),(0,2)]
        self.assertEqual(self.g._quadrilateral_area(ccs), 2.0)


class TestDelwaqHydFile(unittest.TestCase):

    def setUp(self):
        self.testfile = os.sep.join(['data','com-small_grid.hyd'])

    def tearDown(self):
        pass

    def test_r__init_sets_attributes(self):
        hyd = DelwaqHydFile(self.testfile)
        self.assertIsNone(hyd.file_created_by)
        self.assertIsNone(hyd.file_creation_date)
        self.assertEqual(hyd.task,'full-coupling')
        self.assertTrue(hyd.curvilinear)
        self.assertFalse(hyd.zlayers)
        self.assertEqual(hyd.hagg, 'active-only')
        self.assertFalse(hyd.minvdiff)
        self.assertEqual(hyd.vdiff, 'calculated')
        self.assertEqual(hyd.hydreft, datetime(2015,4,28,0, 0,0))
        self.assertEqual(hyd.hydstrt, datetime(2015,4,28,0, 0,0))
        self.assertEqual(hyd.hydstop, datetime(2015,4,28,0,10,0))
        self.assertEqual(hyd.hydstep, timedelta(  0,60))
        self.assertEqual(hyd.conreft, datetime(2015,4,28,0, 0,0))
        self.assertEqual(hyd.constrt, datetime(2015,4,28,0, 0,0))
        self.assertEqual(hyd.constop, datetime(2015,4,28,0,10,0))
        self.assertEqual(hyd.constep, timedelta(  0,60))
        self.assertEqual(hyd.mmax, 8)
        self.assertEqual(hyd.nmax, 7)
        self.assertEqual(hyd.kmaxhd, 2)
        self.assertEqual(hyd.kmaxwq, 2)
        self.assertEqual(hyd.datfile, 'com-small_grid.dat')
        self.assertEqual(hyd.dwqfile, '')
        self.assertEqual(hyd.lgafile, 'com-small_grid.lga')
        self.assertEqual(hyd.ccofile, 'com-small_grid.cco')
        self.assertEqual(hyd.volfile, 'com-small_grid.vol')
        self.assertEqual(hyd.arefile, 'com-small_grid.are')
        self.assertEqual(hyd.flofile, 'com-small_grid.flo')
        self.assertEqual(hyd.poifile, 'com-small_grid.poi')
        self.assertEqual(hyd.lenfile, 'com-small_grid.len')
        self.assertEqual(hyd.salfile, '')
        self.assertEqual(hyd.temfile, '')
        self.assertEqual(hyd.vdffile, 'com-small_grid.vdf')
        self.assertEqual(hyd.srffile, 'com-small_grid.srf')
        self.assertEqual(hyd.dpsfile, 'com-small_grid.dps')
        self.assertEqual(hyd.lgtfile, 'com-small_grid.lgt')
        self.assertEqual(hyd.srcfile, 'com-small_grid.src')
        self.assertEqual(hyd.chzfile, 'com-small_grid.chz')
        self.assertEqual(hyd.taufile, 'com-small_grid.tau')
        self.assertEqual(hyd.wlkfile, 'com-small_grid.wlk')
        self.assertEqual(hyd.atrfile, 'com-small_grid.atr')
        self.assertEqual(hyd.minvdiff_upplyr, 0.0)
        self.assertEqual(hyd.minvdiff_lowlyr, 0.0)
        self.assertEqual(hyd.minvdiff_intdep, 0.0)
        self.assertEqual(hyd.constdsp_dir1, 1.0)
        self.assertEqual(hyd.constdsp_dir2, 1.0)
        self.assertEqual(hyd.constdsp_dir3, 0.0000001)
        self.assertSequenceEqual(hyd.hdlayers,(0.5,0.5))
        self.assertSequenceEqual(hyd.wqlayers,(1.0,1.0))

    def test_r_save_as_produces_same_file(self):
        with open(self.testfile, 'r') as f:
            original_content = f.read()
        hyd = DelwaqHydFile(self.testfile)
        filename = '_remove-me-test-file.tmp'
        hyd.save_as(filename)
        with open(filename, 'r') as f:
            generated_content = f.read()
        os.remove(filename)
        self.assertEqual(generated_content, original_content)

    def test_rw_task_setter_raises_valueerror_for_invalid_value(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.task = 'full-coupling'
        with self.assertRaises(ValueError):
            hyd.task = 'wrong_value'

    def test_rw_curvilinear_setter_raises_valueerror_for_invalid_value(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.curvilinear = False
        hyd.curvilinear = True
        with self.assertRaises(ValueError):
            hyd.curvilinear = 'wrong_value'

    def test_rw_zlayers_setter_raises_valueerror_for_invalid_value(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.zlayers = False
        hyd.zlayers = True
        with self.assertRaises(ValueError):
            hyd.zlayers = 'wrong_value'

    def test_rw_hagg_setter_raises_valueerror_for_invalid_value(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.hagg = False
        hyd.hagg = 'active-only'
        hyd.hagg = True
        with self.assertRaises(ValueError):
            hyd.hagg = 'wrong_value'

    def test_rw_minvdiff_setter_raises_valueerror_for_invalid_value(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.minvdiff = False
        hyd.minvdiff = True
        with self.assertRaises(ValueError):
            hyd.minvdiff = 'wrong_value'

    def test_rw_vdiff_setter_raises_valueerror_for_invalid_value(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.vdiff = 'calculated'
        with self.assertRaises(ValueError):
            hyd.vdiff = 'wrong_value'

    def test_rw_hydreft_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.hydreft = datetime(2000,1,1,0,0,0)
        with self.assertRaises(TypeError):
            hyd.hydreft = 'wrong_value'

    def test_rw_hydstrt_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.hydstrt = datetime(2000,1,1,0,0,0)
        with self.assertRaises(TypeError):
            hyd.hydstrt = 'wrong_value'

    def test_rw_hydstop_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.hydstop = datetime(2000,1,1,0,0,0)
        with self.assertRaises(TypeError):
            hyd.hydstop = 'wrong_value'

    def test_rw_hydstep_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.hydstep = timedelta(0)
        with self.assertRaises(TypeError):
            hyd.hydstep = 'wrong_value'

    def test_rw_conreft_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.conreft = datetime(2000,1,1,0,0,0)
        with self.assertRaises(TypeError):
            hyd.conreft = 'wrong_value'

    def test_rw_constrt_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.constrt = datetime(2000,1,1,0,0,0)
        with self.assertRaises(TypeError):
            hyd.constrt = 'wrong_value'

    def test_rw_constop_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.constop = datetime(2000,1,1,0,0,0)
        with self.assertRaises(TypeError):
            hyd.constop = 'wrong_value'

    def test_rw_constep_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.constep = timedelta(0)
        with self.assertRaises(TypeError):
            hyd.constep = 'wrong_value'

    def test_rw_mmax_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.mmax = 10
        with self.assertRaises(TypeError):
            hyd.mmax = 'wrong_value'

    def test_rw_nmax_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.nmax = 10
        with self.assertRaises(TypeError):
            hyd.nmax = 'wrong_value'

    def test_rw_kmaxhd_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.kmaxhd = 10
        with self.assertRaises(TypeError):
            hyd.kmaxhd = 'wrong_value'

    def test_rw_kmaxhd_setter_resets_and_resizes_hdlayers(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd._hdlayers = [0.5,0.5]
        hyd.kmaxhd = 4
        self.assertSequenceEqual(hyd.hdlayers, [0.25, 0.25, 0.25, 0.25])

    def test_rw_kmaxwq_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.kmaxwq = 10
        with self.assertRaises(TypeError):
            hyd.kmaxwq = 'wrong_value'

    def test_rw_kmaxwq_setter_resets_and_resizes_wqlayers(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd._wqlayers = [1,1]
        hyd.kmaxwq = 4
        self.assertSequenceEqual(hyd.wqlayers, [1, 2, 3, 4])

    def test_rw_file_setters_raise_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.datfile = 'string'
        hyd.dwqfile = 'string'
        hyd.lgafile = 'string'
        hyd.ccofile = 'string'
        hyd.volfile = 'string'
        hyd.arefile = 'string'
        hyd.flofile = 'string'
        hyd.poifile = 'string'
        hyd.lenfile = 'string'
        hyd.salfile = 'string'
        hyd.temfile = 'string'
        hyd.vdffile = 'string'
        hyd.srffile = 'string'
        hyd.dpsfile = 'string'
        hyd.lgtfile = 'string'
        hyd.srcfile = 'string'
        hyd.chzfile = 'string'
        hyd.taufile = 'string'
        hyd.wlkfile = 'string'
        hyd.atrfile = 'string'
        with self.assertRaises(TypeError):
            hyd.datfile = 0
            hyd.dwqfile = 0
            hyd.lgafile = 0
            hyd.ccofile = 0
            hyd.volfile = 0
            hyd.arefile = 0
            hyd.flofile = 0
            hyd.poifile = 0
            hyd.lenfile = 0
            hyd.salfile = 0
            hyd.temfile = 0
            hyd.vdffile = 0
            hyd.srffile = 0
            hyd.dpsfile = 0
            hyd.lgtfile = 0
            hyd.srcfile = 0
            hyd.chzfile = 0
            hyd.taufile = 0
            hyd.wlkfile = 0
            hyd.atrfile = 0

    def test_rw_minvdiff_upplyr_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.minvdiff_upplyr = 10
        hyd.minvdiff_upplyr = 10.0
        with self.assertRaises(TypeError):
            hyd.minvdiff_upplyr = 'wrong_value'

    def test_rw_minvdiff_lowlyr_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.minvdiff_lowlyr = 10
        hyd.minvdiff_lowlyr = 10.0
        with self.assertRaises(TypeError):
            hyd.minvdiff_lowlyr = 'wrong_value'

    def test_rw_minvdiff_intdep_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.minvdiff_intdep = 10
        hyd.minvdiff_intdep = 10.0
        with self.assertRaises(TypeError):
            hyd.minvdiff_intdep = 'wrong_value'

    def test_rw_constdsp_dir1_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.constdsp_dir1 = 10
        hyd.constdsp_dir1 = 10.0
        with self.assertRaises(TypeError):
            hyd.constdsp_dir1 = 'wrong_value'

    def test_rw_constdsp_dir2_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.constdsp_dir2 = 10
        hyd.constdsp_dir2 = 10.0
        with self.assertRaises(TypeError):
            hyd.constdsp_dir2 = 'wrong_value'

    def test_rw_constdsp_dir3_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.constdsp_dir3 = 10
        hyd.constdsp_dir3 = 10.0
        with self.assertRaises(TypeError):
            hyd.constdsp_dir3 = 'wrong_value'

    def test_rw_hdlayers_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.kmaxhd   = 1
        hyd.hdlayers = 10
        hyd.hdlayers = 10.0
        hyd.kmaxhd   = 3
        hyd.hdlayers = [1,2,3]
        with self.assertRaises(TypeError):
            hyd.hdlayers = (1,2,3)

    def test_rw_hdlayers_setter_raises_valueerror_for_invalid_size(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.kmaxhd   = 2
        with self.assertRaises(ValueError):
            hyd.hdlayers = [1,2,3]

    def test_rw_wqlayers_setter_raises_typeerror_for_invalid_type(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.kmaxwq   = 1
        hyd.wqlayers = 10
        hyd.wqlayers = 10.0
        hyd.kmaxwq   = 3
        hyd.wqlayers = [1,2,3]
        with self.assertRaises(TypeError):
            hyd.wqlayers = (1,2,3)

    def test_rw_wqlayers_setter_raises_valueerror_for_invalid_size(self):
        hyd = DelwaqHydFile(self.testfile, 'r+')
        hyd.kmaxwq   = 2
        with self.assertRaises(ValueError):
            hyd.wqlayers = [1,2,3]


class TestDelwaqLenFile(unittest.TestCase):

    def setUp(self):
        self.testfile = '_testfile.len'
        self.noq1 = 3
        self.noq2 = 4
        self.noq3 = 5
        self.noq = self.noq1 + self.noq2 + self.noq3
        with open(self.testfile, 'wb') as f:
            f.write(struct.pack('i', self.noq))
            for i in range(self.noq1):
                f.write(struct.pack('f', 1100 + i))
                f.write(struct.pack('f', 1200 + i))
            for i in range(self.noq2):
                f.write(struct.pack('f', 2100 + i))
                f.write(struct.pack('f', 2200 + i))
            for i in range(self.noq3):
                f.write(struct.pack('f', 3100 + i))
                f.write(struct.pack('f', 3200 + i))
        self.values = np.array(
            [[ 1100.0, 1200.0],
             [ 1101.0, 1201.0],
             [ 1102.0, 1202.0],
             [ 2100.0, 2200.0],
             [ 2101.0, 2201.0],
             [ 2102.0, 2202.0],
             [ 2103.0, 2203.0],
             [ 3100.0, 3200.0],
             [ 3101.0, 3201.0],
             [ 3102.0, 3202.0],
             [ 3103.0, 3203.0],
             [ 3104.0, 3204.0]])

    def tearDown(self):
        os.remove(self.testfile)
        time.sleep(0.001)

    def test_r__init_sets_attributes(self):
        dlf = DelwaqLenFile(self.testfile)
        self.assertEqual(dlf.noq, self.noq)
        self.assertEqual(dlf._secdim, 2)

    def test_rw_init_sets_attributes(self):
        dlf = DelwaqLenFile(self.testfile, 'r+')
        self.assertEqual(dlf.noq, self.noq)
        self.assertEqual(dlf._secdim, 2)

    def test_w__init_sets_dimensions_and_prefills_file(self):
        dlf = DelwaqLenFile(self.testfile, 'w', noq=10)
        self.assertEqual(dlf.noq, 10)
        dlf = DelwaqLenFile(self.testfile)

    def test_r__get_item_returns_slices(self):
        dlf = DelwaqLenFile(self.testfile)
        self.assertTrue( (dlf[0,:] == self.values[0,:]).all() )
        self.assertTrue( (dlf[self.noq-1,:] == self.values[self.noq-1,:]).all() )
        self.assertTrue( (dlf[:,:] == self.values[:,:]).all() )
        self.assertTrue( (dlf[:,0] == self.values[:,0]).all() )
        self.assertTrue( (dlf[:,1] == self.values[:,1]).all() )
        self.assertTrue( (dlf[2:5,1] == self.values[2:5,1]).all() )

    def test_r__get_item_raises_indexerror_when_index_out_of_bounds(self):
        dlf = DelwaqLenFile(self.testfile)
        with self.assertRaises(IndexError):
            dlf[self.noq,:]

    def test_r__set_item_raises_ioerror(self):
        dlf = DelwaqLenFile(self.testfile)
        with self.assertRaises(IOError):
            dlf[0,0] = 0

    def test_rw_set_item_sets_values_for_first_index(self):
        dlf = DelwaqLenFile(self.testfile, 'r+')
        dlf[0,:] = (33.0, 34.0)
        self.assertTrue( (dlf[0,:] == (33.0,34.0)).all() )

    def test_rw_set_item_sets_values_for_last_index(self):
        dlf = DelwaqLenFile(self.testfile, 'r+')
        dlf[self.noq-1,:] = (33.0, 34.0)
        self.assertTrue( (dlf[self.noq-1,:] == (33.0,34.0)).all() )

    def test_rw_set_item_copies_single_value_to_slice(self):
        dlf = DelwaqLenFile(self.testfile, 'r+')
        dlf[1:3,:] = 33.0
        self.assertTrue( (dlf[1:3,:] == np.array([[33.0, 33.0],[33.0, 33.0]])).all() )

    def test_rw_set_item_raises_indexerror_when_index_out_of_bounds(self):
        dlf = DelwaqLenFile(self.testfile, 'r+')
        with self.assertRaises(IndexError):
            dlf[self.noq] = (33.0, 34.0)

    def test_rw_set_item_raises_indexerror_when_value_dimensions_do_not_match(self):
        dlf = DelwaqLenFile(self.testfile, 'r+')
        with self.assertRaises(IndexError):
            dlf[0,0] = (33.0, 34.0)


class TestDelwaqPoiFile(unittest.TestCase):

    def setUp(self):
        self.testfile = '_testfile.poi'
        self.values = np.array(
            [[1, 2, 0, 3],
             [2, 3, 1, 0],
             [4, 5, 0, 6],
             [5, 6, 4, 7],
             [6, 7, 5, 0],
             [9, 10, 0, -2],
             [10, -2, 9, 0],
             [12, 13, 0, -3],
             [13, -3, 12, 0],
             [14, 15, 0, 16],
             [15, 16, 14, 17],
             [16, 17, 15, 18],
             [17, 18, 16, -4],
             [18, -4, 17, 0],
             [20, 21, 0, 22],
             [21, 22, 20, 0],
             [23, 24, 0, 25],
             [24, 25, 23, 26],
             [25, 26, 24, 0],
             [28, 29, 0, -8],
             [29, -8, 28, 0],
             [31, 32, 0, -9],
             [32, -9, 31, 0],
             [33, 34, 0, 35],
             [34, 35, 33, 36],
             [35, 36, 34, 37],
             [36, 37, 35, -10],
             [37, -10, 36, 0],
             [-1, 2, 0, 5],
             [1, 4, 0, 8],
             [2, 5, -1, 0],
             [3, 6, 0, 0],
             [4, 8, 1, 11],
             [7, 9, 0, 12],
             [8, 11, 4, 14],
             [9, 12, 7, 17],
             [10, 13, 0, 18],
             [11, 14, 8, -5],
             [12, 17, 9, 19],
             [13, 18, 10, 0],
             [14, -5, 11, 0],
             [15, -6, 0, 0],
             [17, 19, 12, 0],
             [-7, 21, 0, 24],
             [20, 23, 0, 27],
             [21, 24, -7, 0],
             [22, 25, 0, 0],
             [23, 27, 20, 30],
             [26, 28, 0, 31],
             [27, 30, 23, 33],
             [28, 31, 26, 36],
             [29, 32, 0, 37],
             [30, 33, 27, -11],
             [31, 36, 28, 38],
             [32, 37, 29, 0],
             [33, -11, 30, 0],
             [34, -12, 0, 0],
             [36, 38, 31, 0],
             [1, 20, 0, 0],
             [2, 21, 0, 0],
             [3, 22, 0, 0],
             [4, 23, 0, 0],
             [5, 24, 0, 0],
             [6, 25, 0, 0],
             [7, 26, 0, 0],
             [8, 27, 0, 0],
             [9, 28, 0, 0],
             [10, 29, 0, 0],
             [11, 30, 0, 0],
             [12, 31, 0, 0],
             [13, 32, 0, 0],
             [14, 33, 0, 0],
             [15, 34, 0, 0],
             [16, 35, 0, 0],
             [17, 36, 0, 0],
             [18, 37, 0, 0],
             [19, 38, 0, 0]], dtype=np.int)
        self.noq1 = 28
        self.noq2 = 30
        self.noq3 = 19
        self.noq = self.noq1 + self.noq2 + self.noq3
        with open(self.testfile, 'wb') as f:
            for i in range(self.noq):
                f.write(struct.pack('4i', *self.values[i,:]))

    def tearDown(self):
        os.remove(self.testfile)
        time.sleep(0.001)

    def test_r__init_sets_attributes(self):
        poi = DelwaqPoiFile(self.testfile)
        self.assertEqual(poi.noq, self.noq)
        self.assertEqual(poi._secdim, 4)

    def test_rw_init_sets_attributes(self):
        poi = DelwaqPoiFile(self.testfile, 'r+')
        self.assertEqual(poi.noq, self.noq)
        self.assertEqual(poi._secdim, 4)

    def test_w__init_sets_dimensions_and_prefills_file(self):
        poi = DelwaqPoiFile(self.testfile, 'w', noq=10)
        self.assertEqual(poi.noq, 10)

    def test_r__get_item_returns_slices(self):
        poi = DelwaqPoiFile(self.testfile)
        self.assertTrue( (poi[0,:] == self.values[0,:]).all() )
        self.assertTrue( (poi[self.noq-1,:] == self.values[self.noq-1,:]).all() )
        self.assertTrue( (poi[:,:] == self.values[:,:]).all() )
        self.assertTrue( (poi[:,0] == self.values[:,0]).all() )
        self.assertTrue( (poi[:,1] == self.values[:,1]).all() )
        self.assertTrue( (poi[2:5,1] == self.values[2:5,1]).all() )

    def test_r__get_item_raises_indexerror_when_index_out_of_bounds(self):
        poi = DelwaqPoiFile(self.testfile)
        with self.assertRaises(IndexError):
            poi[self.noq,:]

    def test_r__set_item_raises_ioerror(self):
        poi = DelwaqPoiFile(self.testfile)
        with self.assertRaises(IOError):
            poi[0,0] = 0

    def test_rw_set_item_sets_values_for_first_index(self):
        poi = DelwaqPoiFile(self.testfile, 'r+')
        poi[0,:] = (33,34,35,36)
        self.assertTrue( (poi[0,:] == (33,34,35,36)).all() )

    def test_rw_set_item_sets_values_for_last_index(self):
        poi = DelwaqPoiFile(self.testfile, 'r+')
        poi[self.noq-1,:] = (33,34,35,36)
        self.assertTrue( (poi[self.noq-1,:] == (33,34,35,36)).all() )

    def test_rw_set_item_copies_single_value_to_slice(self):
        poi = DelwaqPoiFile(self.testfile, 'r+')
        poi[1:3,:] = 33
        self.assertTrue( (poi[1:3,:] == np.array([[33,33,33,33],[33,33,33,33]])).all() )

    def test_rw_set_item_raises_indexerror_when_index_out_of_bounds(self):
        poi = DelwaqPoiFile(self.testfile, 'r+')
        with self.assertRaises(IndexError):
            poi[self.noq] = (33.0, 34.0)

    def test_rw_set_item_raises_indexerror_when_value_dimensions_do_not_match(self):
        poi = DelwaqPoiFile(self.testfile, 'r+')
        with self.assertRaises(IndexError):
            poi[0,0] = (33.0, 34.0)


class TestDelwaqSrfFile(unittest.TestCase):

    def setUp(self):
        self.testfile = '_testfile.srf'
        self.m = 4
        self.n = 3
        self.nosegh = 10
        with open(self.testfile, 'wb') as f:
            f.write(struct.pack('i', self.n))
            f.write(struct.pack('i', self.m))
            f.write(struct.pack('i', self.nosegh))
            f.write(struct.pack('i', self.nosegh))
            f.write(struct.pack('i', self.nosegh))
            f.write(struct.pack('i', 0))
            for i in range(self.nosegh):
                f.write(struct.pack('f', i))

    def tearDown(self):
        os.remove(self.testfile)
        time.sleep(0.001)

    def test_r__init_sets_attributes(self):
        srf = DelwaqSrfFile(self.testfile)
        self.assertEqual(srf.mmax, self.m)
        self.assertEqual(srf.nmax, self.n)
        self.assertEqual(srf.nosegh, self.nosegh)

    def test_rw_init_sets_attributes(self):
        srf = DelwaqSrfFile(self.testfile, 'r+')
        self.assertEqual(srf.mmax, self.m)
        self.assertEqual(srf.nmax, self.n)
        self.assertEqual(srf.nosegh, self.nosegh)

    def test_w__init_sets_dimensions_and_prefills_file(self):
        srf = DelwaqSrfFile(self.testfile, 'w', nosegh=10)
        self.assertEqual(srf.mmax, 0)
        self.assertEqual(srf.nmax, 0)
        self.assertEqual(srf.nosegh, 10)
        srf = DelwaqSrfFile(self.testfile)

    def test_r__mmax_setter_raises_ioerror(self):
        srf = DelwaqSrfFile(self.testfile)
        with self.assertRaises(IOError):
            srf.mmax = 1

    def test_r__mmax_setter_raises_ioerror(self):
        srf = DelwaqSrfFile(self.testfile)
        with self.assertRaises(IOError):
            srf.nmax = 1

    def test_rw_mmax_setter_sets_mmax_in_file(self):
        srf = DelwaqSrfFile(self.testfile, 'r+')
        srf.mmax = 1
        self.assertEqual(srf._mmax, 1)
        srf = DelwaqSrfFile(self.testfile)
        self.assertEqual(srf.mmax, 1)

    def test_rw_nmax_setter_sets_nmax_in_file(self):
        srf = DelwaqSrfFile(self.testfile, 'r+')
        srf.nmax = 1
        self.assertEqual(srf._nmax, 1)
        srf = DelwaqSrfFile(self.testfile)
        self.assertEqual(srf.nmax, 1)

    def test_r__get_item_returns_value_for_first_index(self):
        srf = DelwaqSrfFile(self.testfile)
        self.assertEqual(srf[0], 0.0)

    def test_r__get_item_returns_value_for_last_index(self):
        srf = DelwaqSrfFile(self.testfile)
        self.assertEqual(srf[self.nosegh-1], 9.0)

    def test_r__get_item_returns_1d_array_for_slice(self):
        srf = DelwaqSrfFile(self.testfile)
        self.assertTrue( (srf[1:3] == np.array([1.0,2.0])).all() )

    def test_r__get_item_raises_indexerror_when_index_out_of_bounds(self):
        srf = DelwaqSrfFile(self.testfile)
        with self.assertRaises(IndexError):
            srf[self.nosegh]
        with self.assertRaises(IndexError):
            srf[1:self.nosegh+1]

    def test_r__set_item_raises_ioerror(self):
        srf = DelwaqSrfFile(self.testfile)
        with self.assertRaises(IOError):
            srf[0] = 0

    def test_rw_set_item_sets_value_for_first_index(self):
        srf = DelwaqSrfFile(self.testfile, 'r+')
        srf[0] = 33.0
        self.assertEqual(srf[0], 33.0)

    def test_rw_set_item_sets_value_for_last_index(self):
        srf = DelwaqSrfFile(self.testfile, 'r+')
        srf[self.nosegh-1] = 33.0
        self.assertEqual(srf[self.nosegh-1], 33.0)

    def test_rw_set_item_sets_values_for_slice(self):
        srf = DelwaqSrfFile(self.testfile, 'r+')
        dat = [33.0 for i in range(self.nosegh)]
        srf[:] = dat
        self.assertTrue( (srf[:] == dat).all() )

    def test_rw_set_item_copies_single_value_to_slice(self):
        srf = DelwaqSrfFile(self.testfile, 'r+')
        srf[:] = 33.0
        self.assertTrue( (srf[:] == [33.0 for i in range(self.nosegh)]).all() )

    def test_rw_set_item_raises_indexerror_when_index_out_of_bounds(self):
        srf = DelwaqSrfFile(self.testfile, 'r+')
        with self.assertRaises(IndexError):
            srf[self.nosegh] = 33.0
        with self.assertRaises(IndexError):
            srf[1:self.nosegh+1] = 33.0

    def test_rw_set_item_raises_indexerror_when_value_dimensions_do_not_match(self):
        srf = DelwaqSrfFile(self.testfile, 'r+')
        with self.assertRaises(IndexError):
            srf[0] = [33.0, 34.0]


class TestDelwaqVolFile(unittest.TestCase):

    def setUp(self):
        # make test file
        self.testfile = '_testfile.vol'
        self.notim = 4
        self.noseg = 9
        with open(self.testfile, 'wb') as f:
            for t in range(self.notim):
                f.write(struct.pack('i',t*100))
                for val in range(self.noseg):
                    f.write(struct.pack('f',val+t*100))
        self.times  = [0,100,200,300]
        self.values = np.array(
            [[   0.0,    1.0,    2.0,    3.0,    4.0,    5.0,    6.0,    7.0,    8.0,],
             [ 100.0,  101.0,  102.0,  103.0,  104.0,  105.0,  106.0,  107.0,  108.0,],
             [ 200.0,  201.0,  202.0,  203.0,  204.0,  205.0,  206.0,  207.0,  208.0,],
             [ 300.0,  301.0,  302.0,  303.0,  304.0,  305.0,  306.0,  307.0,  308.0,]])

    def tearDown(self):
        os.remove(self.testfile)
        time.sleep(0.001)

    def test_r__init_sets_attributes(self):
        vol = DelwaqVolFile(self.testfile, self.noseg)
        self.assertEqual(vol.notim, self.notim)
        self.assertEqual(vol.noseg, self.noseg)

    def test_rw_init_sets_attributes(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        self.assertEqual(vol.notim, self.notim)
        self.assertEqual(vol.noseg, self.noseg)

    def test_w__init_sets_dimensions_and_prefills_file(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'w', notim=13)
        self.assertEqual(vol.notim, 13)
        self.assertEqual(vol.noseg, self.noseg)
        # Make sure the file is valid by opening it again in r mode
        vol = DelwaqVolFile(self.testfile, self.noseg)

    def test_r__times_returns_all_timestamps(self):
        vol = DelwaqVolFile(self.testfile, self.noseg)
        self.assertSequenceEqual(vol.times, self.times)

    def test_r__times_setter_raises_ioerror(self):
        vol = DelwaqVolFile(self.testfile, self.noseg)
        with self.assertRaises(IOError):
            vol.times = self.times

    def test_rw_times_setter_overwrites_existing_timestamps(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        times = [t*101 for t in range(self.notim)]
        vol.times = times
        self.assertSequenceEqual(vol.times, times)

    def test_rw_times_setter_raises_valueerror_when_nr_values_neq_nb_times(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        times = [t*101 for t in range(self.notim+2)]
        with self.assertRaises(ValueError):
            vol.times = times

    def test_r__get_item_returns_values(self):
        vol = DelwaqVolFile(self.testfile, self.noseg)
        self.assertEqual(vol[0,0], self.values[0,0])
        self.assertEqual(vol[1,2], self.values[1,2])
        self.assertEqual(vol[self.notim-1, self.noseg-1], self.values[self.notim-1, self.noseg-1])
        self.assertTrue((vol[1,2:4] == self.values[1,2:4]).all() )
        self.assertTrue((vol[1:3,2] == self.values[1:3,2]).all() )
        self.assertTrue((vol[1:3,2:4] == self.values[1:3,2:4]).all() )

    def test_r__get_item_raises_indexerror_when_time_index_out_of_bounds(self):
        vol = DelwaqVolFile(self.testfile, self.noseg)
        with self.assertRaises(IndexError):
            vol[self.notim, 0]
        with self.assertRaises(IndexError):
            vol[1:self.notim+1, 0]

    def test_r__get_item_raises_indexerror_when_segment_index_out_of_bounds(self):
        vol = DelwaqVolFile(self.testfile, self.noseg)
        with self.assertRaises(IndexError):
            vol[0, self.noseg]
        with self.assertRaises(IndexError):
            vol[0, 1:self.noseg+1]

    def test_r__set_item_raises_ioerror(self):
        vol = DelwaqVolFile(self.testfile, self.noseg)
        with self.assertRaises(IOError):
            vol[0, 0] = 0

    def test_rw_set_item_sets_value_for_index(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        dat = 33.0
        vol[1,2] = dat
        self.assertEqual(dat, vol[1,2])

    def test_rw_set_item_sets_1d_array_for_segment_slice(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        dat = np.array([33.0,34.0])
        vol[1,2:4] = dat
        self.assertTrue( (dat == vol[1,2:4]).all() )

    def test_rw_set_item_sets_1d_array_for_time_slice(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        dat = np.array([33.0,34.0])
        vol[1:3,2] = dat
        self.assertTrue( (dat == vol[1:3,2]).all() )

    def test_rw_set_item_sets_2d_array_for_time_segment_slice(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        dat = np.array([[102.0,103.0],[202.0,203.0]])
        vol[1:3,2:4] = dat
        self.assertTrue( (dat == vol[1:3,2:4]).all() )

    def test_rw_set_item_raises_indexerror_when_time_index_out_of_bounds(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        with self.assertRaises(IndexError):
            vol[self.notim, 0] = 33.0
        with self.assertRaises(IndexError):
            vol[1:self.notim+1, 0] = 33.0

    def test_rw_set_item_raises_indexerror_when_segment_index_out_of_bounds(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        with self.assertRaises(IndexError):
            vol[0, self.noseg] = 33.0
        with self.assertRaises(IndexError):
            vol[0, 1:self.noseg+1] = 33.0

    def test_rw_set_item_raises_indexerror_when_value_dimensions_do_not_match(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        with self.assertRaises(IndexError):
            vol[0, self.noseg] = [33.0, 34.0]

    def test_rw_set_item_copies_single_value_to_slice(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r+')
        vol[1:3,2:5] = 33.0
        self.assertTrue( (np.array([[33.0, 33.0, 33.0],[33.0, 33.0, 33.0]]) == vol[1:3,2:5]).all() )

    def test_r__iter_times_yields_times(self):
        vol = DelwaqVolFile(self.testfile, self.noseg, 'r')
        for i, t in enumerate(vol.iter_times()):
            self.assertEqual(i*100, t)

    def test_r__dump_writes_contents_to_csv(self):
        vol = DelwaqVolFile(self.testfile, self.noseg)
        exp = "# CSV dump of file: '{}'\n".format(vol.abspath)
        exp+= "# Number of times        : {}\n".format(self.notim)
        exp+= "# Number of segments     : {}\n".format(self.noseg)
        exp+= "# First column : times\n"
        exp+= "# Other columns: values per segment\n"
        exp+= "0;0.000000e+00;1.000000e+00;2.000000e+00;3.000000e+00;4.000000e+00;5.000000e+00;6.000000e+00;7.000000e+00;8.000000e+00\n"
        exp+= "100;1.000000e+02;1.010000e+02;1.020000e+02;1.030000e+02;1.040000e+02;1.050000e+02;1.060000e+02;1.070000e+02;1.080000e+02\n"
        exp+= "200;2.000000e+02;2.010000e+02;2.020000e+02;2.030000e+02;2.040000e+02;2.050000e+02;2.060000e+02;2.070000e+02;2.080000e+02\n"
        exp+= "300;3.000000e+02;3.010000e+02;3.020000e+02;3.030000e+02;3.040000e+02;3.050000e+02;3.060000e+02;3.070000e+02;3.080000e+02\n"
        vol.dump()
        csvname = vol.path+'.csv'
        with open(csvname, 'r') as f:
            content = f.read()
        self.assertEqual(content, exp)
        os.remove(csvname)


class TestDelwaqDmoFile(unittest.TestCase):

    def setUp(self):
        # make test file
        self.testfile = '_testfile.dmo'
        self.count = 2
        self.areas = ['ObsArea 001', 'ObsArea 002']
        self.d = OrderedDict()
        self.d['ObsArea 001'] = [1,2,3,4,5,6,7]
        self.d['ObsArea 002'] = [10,20,30]
        with open(self.testfile, 'w') as f:
            f.write("         2\n")
            f.write(" 'ObsArea 001'           7\n")
            f.write("         1         2         3         4         5\n")
            f.write("         6         7\n")
            f.write(" 'ObsArea 002'           3\n")
            f.write("        10        20        30\n")

    def tearDown(self):
        os.remove(self.testfile)
        time.sleep(0.001)

    def test_r__init_loads_data(self):
        f = DelwaqDmoFile(self.testfile)
        self.assertEqual(f.count, self.count)
        self.assertEqual(f.areas, self.areas)
        self.assertSequenceEqual(f._d, self.d)

    def test_r__getitem_returns_segmens_of_area(self):
        f = DelwaqDmoFile(self.testfile)
        self.assertSequenceEqual(f['ObsArea 002'], [10,20,30])

    def test_r__iter_yields_areas_segments(self):
        f = DelwaqDmoFile(self.testfile)
        areas = []
        segs = []
        for area, _segs in f:
            areas.append(area)
            segs.append(_segs)
        self.assertSequenceEqual(areas, self.areas)
        self.assertSequenceEqual(segs[0], [1,2,3,4,5,6,7])
        self.assertSequenceEqual(segs[1], [10,20,30])


class TestDelwaqParFile(unittest.TestCase):

    def setUp(self):
        # make test file
        self.testfile = '_testfile.par'
        self.nopar = 2
        self.noseg = 3
        self.par1 = [1,2,3]
        self.par2 = [10,20,30]
        with open(self.testfile, 'wb') as f:
            f.write(struct.pack('i',0))
            f.write(struct.pack('2f',self.par1[0],self.par2[0]))
            f.write(struct.pack('2f',self.par1[1],self.par2[1]))
            f.write(struct.pack('2f',self.par1[2],self.par2[2]))

    def tearDown(self):
        os.remove(self.testfile)
        time.sleep(0.001)

    def test_r__init_with_sets_attributes(self):
        f = DelwaqParFile(self.testfile, nopar=self.nopar)
        self.assertEqual(f.nopar, self.nopar)
        self.assertEqual(f.noseg, self.noseg)

    def test_w__init_sets_dimensions_and_prefills_file(self):
        f = DelwaqParFile(self.testfile, nopar=self.nopar, noseg=self.noseg, mode='w')
        self.assertEqual(f.nopar, self.nopar)
        self.assertEqual(f.noseg, self.noseg)
        self.assertEqual(f.bytes, (self.noseg * self.nopar) + 1)

    def test_w__init_raises_valueerror_if_missing_dimensions(self):
        with self.assertRaises(ValueError):
            f = DelwaqParFile(self.testfile, mode='w')
        with self.assertRaises(ValueError):
            f = DelwaqParFile(self.testfile, mode='w', noseg=3)
        with self.assertRaises(ValueError):
            f = DelwaqParFile(self.testfile, mode='w', nopar=2)

    def test_r__get_item_returns_values(self):
        f = DelwaqParFile(self.testfile, nopar=self.nopar)
        self.assertTrue( (f[:,0] == self.par1).all() )

    def test_w__set_item_sets_values(self):
        f = DelwaqParFile(self.testfile, mode='w', noseg=self.noseg, nopar=self.nopar)
        values = [100 + i for i in range(self.noseg)]
        f[:,0] = values
        f = DelwaqParFile(self.testfile, nopar=self.nopar)
        self.assertTrue( (f[:,0] == values).all() )


class TestDelwaqHisFile(unittest.TestCase):

    def setUp(self):
        self.testfile = "test.his"
        with open(self.testfile, 'wb') as f:
            f.write(b'                                        ')
            f.write(b'                                        ')
            f.write(b'                                        ')
            f.write(b'T0: 2014.02.01 00:00:00  (scu=       1s)')
            self.reftime = datetime(2014,2,1,0,0,0)
            self.nosub = 3
            f.write(struct.pack('i',self.nosub))
            self.noloc = 2
            f.write(struct.pack('i',self.noloc))
            self.subs = ['OXY', 'SURF', 'LocalDepth']
            for sub in self.subs:
                f.write( ("%-20s"%sub).encode())
            self.locs = ['area_1', 'area_2']
            self._locindxs = [2,1]
            for i, loc in enumerate(self.locs):
                f.write(struct.pack('i',self._locindxs[i]))
                f.write( ("%-20s"%loc).encode())
            self.header_size = f.tell()
            self.notim = 7
            self.s1a1  = [10 + i for i in range(self.notim)]
            self.s2a1  = [20 + i for i in range(self.notim)]
            self.s3a1  = [30 + i for i in range(self.notim)]
            self.s1a2  = [40 + i for i in range(self.notim)]
            self.s2a2  = [50 + i for i in range(self.notim)]
            self.s3a2  = [60 + i for i in range(self.notim)]
            self.times = [t * 600 for t in range(self.notim)]
            self.dates = [self.reftime + timedelta(seconds=600*i) for i in range(self.notim)]
            for it, t in enumerate(self.times):
                f.write(struct.pack('i',t))
                # area 2 comes first
                f.write(struct.pack('f',self.s1a2[it]))
                f.write(struct.pack('f',self.s2a2[it]))
                f.write(struct.pack('f',self.s3a2[it]))
                # then area 1
                f.write(struct.pack('f',self.s1a1[it]))
                f.write(struct.pack('f',self.s2a1[it]))
                f.write(struct.pack('f',self.s3a1[it]))

    def tearDown(self):
        os.remove(self.testfile)
        time.sleep(0.001)

    def test_r__init_sets_attributes(self):
        f = DelwaqHisFile(self.testfile)
        self.assertEqual(f.reftime, self.reftime)
        self.assertEqual(f.notim, self.notim)
        self.assertEqual(f.nosub, self.nosub)
        self.assertEqual(f.noloc, self.noloc)
        self.assertSequenceEqual(f._subnames, self.subs)
        self.assertSequenceEqual(f._locnames, self.locs)
        self.assertEqual(f._header_size, self.header_size)
        self.assertEqual(f._locindxs, self._locindxs)

    def test_w__init_sets_dimensions_and_prefills_file(self):
        # create file
        f = DelwaqHisFile(self.testfile, mode='w', notim=7, nosub=2, noloc=3)
        # repopen file in read mode and check it's healthy
        f = DelwaqHisFile(self.testfile)
        self.assertEqual(f.notim, 7)
        self.assertEqual(f.nosub, 2)
        self.assertEqual(f.noloc, 3)
        self.assertEqual(f.reftime, datetime(2000,1,1,0,0,0))
        self.assertSequenceEqual(f.subs, ['Substance_00001', 'Substance_00002'])
        self.assertSequenceEqual(f.locs, ['Location_00001', 'Location_00002', 'Location_00003'])
        self.assertSequenceEqual(f.times,[0,1,2,3,4,5,6])

    def test_w__init_raises_valueerror_if_missing_dimensions(self):
        with self.assertRaises(ValueError):
            f = DelwaqHisFile(self.testfile, 'w')

    def test_r__times_returns_list_of_times(self):
        f = DelwaqHisFile(self.testfile)
        self.assertSequenceEqual( f.times, self.times)

    def test_r__dates_returns_list_of_datetimes(self):
        f = DelwaqHisFile(self.testfile)
        self.assertSequenceEqual( f.dates, self.dates)

    def test_r__subs_returns_list_of_substance_namces(self):
        f = DelwaqHisFile(self.testfile)
        self.assertSequenceEqual( f.subs, self.subs)

    def test_r__locs_returns_list_of_location_names(self):
        f = DelwaqHisFile(self.testfile)
        self.assertSequenceEqual( f.locs, self.locs)

    def test_r__location_key_to_index_returns_index_from_index(self):
        f = DelwaqHisFile(self.testfile)
        self.assertEqual(f._location_key_to_index(0), 1)

    def test_r__location_key_to_index_returns_index_from_name(self):
        f = DelwaqHisFile(self.testfile)
        exp = self._locindxs[self.locs.index('area_1')]-1
        self.assertEqual(f._location_key_to_index('area_1'), exp)

    def test_r__getitem_raises_indexerror_if_number_of_dimensions_is_not_3(self):
        f = DelwaqHisFile(self.testfile)
        with self.assertRaises(IndexError):
            a = f[:,:]

    def test_r__getitem_returns_single_value(self):
        f = DelwaqHisFile(self.testfile)
        self.assertEqual(f['SURF','area_1',3], self.s2a1[3])
        self.assertTrue(isinstance(f['SURF','area_1',3], np.float64))

    def test_r__getitem_returns_single_array(self):
        f = DelwaqHisFile(self.testfile)
        self.assertTrue( (f['SURF','area_1',:] == self.s2a1).all() )

    def test_rw_setitem_writes_single_value(self):
        f = DelwaqHisFile(self.testfile)
        f['SURF','area_1',3] = -33.0
        f = DelwaqHisFile(self.testfile)
        self.assertEqual(f['SURF','area_1',3], -33.0)

    def test_rw_setitem_writes_single_value_to_array(self):
        f = DelwaqHisFile(self.testfile)
        f['SURF','area_1',:] = -33.0
        f = DelwaqHisFile(self.testfile)
        self.assertTrue( (f['SURF','area_1',:] == [-33.0 for i in range(self.notim)]).all() )

    def test_rw_setitem_writes_array_to_array(self):
        f = DelwaqHisFile(self.testfile)
        f['SURF','area_1',1:3] = [-33.0, -34.0]
        f = DelwaqHisFile(self.testfile)
        self.assertTrue( (f['SURF','area_1',1:3] == [-33.0, -34.0]).all() )

    def test_rw_set_item_raises_indexerror_when_index_out_of_bounds(self):
        f = DelwaqHisFile(self.testfile)
        with self.assertRaises(IndexError):
            f['SURF','area_1',self.notim] = -33.0

    def test_rw_set_item_raises_indexerror_when_value_dimensions_do_not_match(self):
        f = DelwaqHisFile(self.testfile)
        with self.assertRaises(IndexError):
            f['SURF','area_1',1:3] = [-33.0, -34.0, -35.0]

    def test_rw_times_setter_writes_values_to_files(self):
        f = DelwaqHisFile(self.testfile, mode='r+')
        times = [t+1 for t in f.times]
        f.times = times
        f = DelwaqHisFile(self.testfile)
        self.assertSequenceEqual(f.times, times)

    def test_rw_subs_setter_writes_values_to_files(self):
        f = DelwaqHisFile(self.testfile, mode='r+')
        subs = [str(i) for i in range(f.nosub)]
        f.subs = subs
        # verify property was updated
        self.assertSequenceEqual(f.subs, subs)
        # verify values were saved to file
        f = DelwaqHisFile(self.testfile)
        self.assertSequenceEqual(f.subs, subs)

    def test_rw_locs_setter_writes_values_to_files(self):
        f = DelwaqHisFile(self.testfile, mode='r+')
        locs = [str(i) for i in range(f.noloc)]
        f.locs = locs
        # verify property was updated
        self.assertSequenceEqual(f.locs, locs)
        self.assertSequenceEqual(f._locindxs, [i+1 for i in range(self.noloc)])
        # verify values were saved to file
        f = DelwaqHisFile(self.testfile)
        self.assertSequenceEqual(f.locs, locs)
        self.assertSequenceEqual(f._locindxs, [i+1 for i in range(self.noloc)])


class TestDelwaqMapFile(unittest.TestCase):

    def setUp(self):
        self.testfile = "d:\\dev\\d3d\\small_agg\\test_hyd\\wqnew.map"
        self.testfile = '_testfile.map'
        with open(self.testfile, 'wb') as f:
            f.write(b'                                        ')
            f.write(b'                                        ')
            f.write(b'                                        ')
            f.write(b'T0: 2014.02.01 00:00:00  (scu=       1s)')
            self.reftime = datetime(2014,2,1,0,0,0)
            self.nosub = 3
            f.write(struct.pack('i',self.nosub))
            self.noseg = 2
            f.write(struct.pack('i',self.noseg))
            self.subs = ['OXY', 'SURF', 'LocalDepth']
            for sub in self.subs:
                f.write( ("%-20s"%sub).encode())
            self.header_size = f.tell()
            self.notim = 7
            self.times = [(t+1) * 3600 for t in range(self.notim)]
            self.sub1seg1  = [10 + i for i in range(self.notim)]
            self.sub2seg1  = [20 + i for i in range(self.notim)]
            self.sub3seg1  = [30 + i for i in range(self.notim)]
            self.sub1seg2  = [40 + i for i in range(self.notim)]
            self.sub2seg2  = [50 + i for i in range(self.notim)]
            self.sub3seg2  = [60 + i for i in range(self.notim)]
            for it, t in enumerate(self.times):
                f.write(struct.pack('i',t))
                f.write(struct.pack('f',self.sub1seg1[it]))
                f.write(struct.pack('f',self.sub2seg1[it]))
                f.write(struct.pack('f',self.sub3seg1[it]))
                f.write(struct.pack('f',self.sub1seg2[it]))
                f.write(struct.pack('f',self.sub2seg2[it]))
                f.write(struct.pack('f',self.sub3seg2[it]))

    def tearDown(self):
        os.remove(self.testfile)
        time.sleep(0.001)

    def test_r__init_sets_attributes(self):
        f = DelwaqMapFile(self.testfile)
        self.assertEqual(f.reftime, self.reftime)
        self.assertEqual(f.notim, self.notim)
        self.assertEqual(f.nosub, self.nosub)
        self.assertEqual(f.noseg, self.noseg)
        self.assertEqual(f._header_size, self.header_size)

    def test_w__init_sets_dimensions_and_prefills_file(self):
        # create file
        f = DelwaqMapFile(self.testfile, mode='w', notim=3, nosub=2, noseg=7)
        # repopen file in read mode and check it's healthy
        f = DelwaqMapFile(self.testfile)
        self.assertEqual(f.notim, 3)
        self.assertEqual(f.nosub, 2)
        self.assertEqual(f.noseg, 7)
        self.assertSequenceEqual(f.subs, ['Substance_00001', 'Substance_00002'])
        self.assertSequenceEqual(f.times,[0,1,2])

    def test_w__init_raises_valueerror_if_missing_dimensions(self):
        with self.assertRaises(ValueError):
            f = DelwaqMapFile(self.testfile, 'w')

    def test_r__times_returns_list_of_times(self):
        f = DelwaqMapFile(self.testfile)
        self.assertSequenceEqual( f.times, self.times)

    def test_r__subs_returns_list_of_substance_namces(self):
        f = DelwaqMapFile(self.testfile)
        self.assertSequenceEqual( f.subs, self.subs)

    def test_r__getitem_raises_indexerror_if_number_of_dimensions_is_not_3(self):
        f = DelwaqMapFile(self.testfile)
        with self.assertRaises(IndexError):
            a = f[:,:]

    def test_r__getitem_returns_single_value(self):
        f = DelwaqMapFile(self.testfile)
        self.assertEqual(f['SURF',0,3], self.sub2seg1[3])
        self.assertTrue(isinstance(f['SURF',0,3], np.float64))

    def test_r__getitem_returns_single_array(self):
        f = DelwaqMapFile(self.testfile)
        self.assertTrue( (f['SURF',0,:] == self.sub2seg1).all() )

    def test_rw_setitem_writes_single_value(self):
        f = DelwaqMapFile(self.testfile)
        f['SURF',0,3] = -33.0
        f = DelwaqMapFile(self.testfile)
        self.assertEqual(f['SURF',0,3], -33.0)

    def test_rw_setitem_writes_single_value_to_array(self):
        f = DelwaqMapFile(self.testfile)
        f['SURF',0,:] = -33.0
        f = DelwaqMapFile(self.testfile)
        self.assertTrue( (f['SURF',0,:] == [-33.0 for i in range(self.notim)]).all() )

    def test_rw_setitem_writes_array_to_array(self):
        f = DelwaqMapFile(self.testfile)
        f['SURF',0,1:3] = [-33.0, -34.0]
        f = DelwaqMapFile(self.testfile)
        self.assertTrue( (f['SURF',0,1:3] == [-33.0, -34.0]).all() )

    def test_rw_set_item_raises_indexerror_when_index_out_of_bounds(self):
        f = DelwaqMapFile(self.testfile)
        with self.assertRaises(IndexError):
            f['SURF',0,self.notim] = -33.0

    def test_rw_set_item_raises_indexerror_when_value_dimensions_do_not_match(self):
        f = DelwaqMapFile(self.testfile)
        with self.assertRaises(IndexError):
            f['SURF',0,1:3] = [-33.0, -34.0, -35.0]

    def test_rw_times_setter_writes_values_to_files(self):
        f = DelwaqMapFile(self.testfile, mode='r+')
        times = [t+1 for t in f.times]
        f.times = times
        f = DelwaqMapFile(self.testfile)
        self.assertSequenceEqual(f.times, times)

    def test_rw_subs_setter_writes_values_to_files(self):
        f = DelwaqMapFile(self.testfile, mode='r+')
        subs = [str(i) for i in range(f.nosub)]
        f.subs = subs
        f = DelwaqMapFile(self.testfile)
        self.assertSequenceEqual(f.subs, subs)


class TestFlowGrid(unittest.TestCase):

    def setUp(self):
        self.g = FlowGrid(None)
        self.g.grd_file = os.sep.join(['data','small_grid.grd'])

    def test_load_sets_coordinate_system(self):
        self.g._load()
        self.assertEqual('Cartesian', self.g.coordinate_system)

    def test_load_sets_missing_value(self):
        self.g._load()
        self.assertEqual(np.float('-9.99999000000000024E+02'), self.g.missing_value)

    def test_load_sets_mmax(self):
        self.g._load()
        self.assertEqual(7, self.g.mmax)

    def test_load_sets_nmax(self):
        self.g._load()
        self.assertEqual(6, self.g.nmax)

    def test_load_sets_xs(self):
        self.g._load()
        exp = np.array(
            [[1.00461666666666645E+03, 2.14201666666666688E+03, 3.27941666666666742E+03, 4.41681666666666388E+03, 5.55421666666666897E+03, 6.69161666666667406E+03, -9.99999000000000024E+02],
             [1.00461666666666645E+03, 2.08958333333333348E+03, 3.17455000000000064E+03, 4.25951666666666461E+03, 5.34448333333333539E+03, 6.42945000000000618E+03, -9.99999000000000024E+02],
             [1.00461666666666645E+03,2.03715000000000009E+03,3.06968333333333385E+03,-9.99999000000000024E+02,5.13475000000000182E+03,6.16728333333333831E+03,-9.99999000000000024E+02],
             [1.00461666666666645E+03,1.98471666666666670E+03,2.96481666666666706E+03,3.94491666666666742E+03,4.92501666666666824E+03,5.90511666666667043E+03,6.88521666666667261E+03],
             [-9.99999000000000024E+02,1.93228333333333330E+03,2.85995000000000027E+03,3.78761666666666724E+03,4.71528333333333467E+03,5.64295000000000255E+03,6.57061666666667043E+03],
             [-9.99999000000000024E+02,-9.99999000000000024E+02,2.75508333333333348E+03,3.63031666666666706E+03,4.50555000000000109E+03,5.38078333333333467E+03, -9.99999000000000024E+02]],
            dtype=np.float)
        exp = np.transpose(exp)
        self.assertTrue( (exp==self.g.xs).all() )

    def test_load_sets_ys(self):
        self.g._load()
        exp = np.array(
            [[7.05416666666666629E+02, 7.69949999999999932E+02, 8.34483333333333235E+02, 8.99016666666661877E+02, 9.63549999999991996E+02, 1.02808333333332212E+03, -9.99999000000000024E+02],
             [1.58468333333333339E+03, 1.64115000000000009E+03, 1.69761666666666679E+03, 1.75408333333333030E+03, 1.81054999999999472E+03, 1.86701666666665915E+03, -9.99999000000000024E+02],
             [2.46395000000000027E+03, 2.51235000000000036E+03, 2.56075000000000045E+03, -9.99999000000000024E+02, 2.65754999999999745E+03, 2.70594999999999618E+03, -9.99999000000000024E+02],
             [3.34321666666666715E+03, 3.38355000000000064E+03, 3.42388333333333412E+03, 3.46421666666666761E+03, 3.50455000000000018E+03, 3.54488333333333321E+03, 3.58521666666666624E+03],
             [-9.99999000000000024E+02, 4.25475000000000091E+03, 4.28701666666666824E+03, 4.31928333333333558E+03, 4.35155000000000291E+03, 4.38381666666667024E+03, 4.41608333333333758E+03],
             [-9.99999000000000024E+02, -9.99999000000000024E+02, 5.15015000000000236E+03, 5.17435000000000400E+03, 5.19855000000000564E+03, 5.22275000000000728E+03, -9.99999000000000024E+02]],
            dtype=np.float)
        exp = np.transpose(exp)
        self.assertTrue( (exp==self.g.ys).all() )


class TestFlowGridDep(unittest.TestCase):

    def setUp(self):
        grd_file = os.sep.join(['data','small_grid.grd'])
        dep_file = os.sep.join(['data','small_dep.dep'])
        self.g = FlowGrid(grd_file, dep=dep_file)

    def test_load_sets_ds(self):
        exp = np.array(
            [[ 1.10E+00,  1.20E+00,  1.30E+00,  1.40E+00,  1.50E+00,  1.60E+00, -9.99E+02],
             [ 2.10E+00,  2.20E+00,  2.30E+00,  2.40E+00,  2.50E+00,  2.60E+00, -9.99E+02],
             [ 3.10E+00,  3.20E+00,  3.30E+00, -9.99E+02,  3.50E+00,  3.60E+00, -9.99E+02],
             [ 4.10E+00,  4.20E+00,  4.30E+00,  4.40E+00,  4.50E+00,  4.60E+00,  4.70E+00],
             [-9.99E+02,  5.20E+00,  5.30E+00,  5.40E+00,  5.50E+00,  5.60E+00,  5.70E+00],
             [-9.99E+02, -9.99E+02,  6.30E+00,  6.40E+00,  6.50E+00,  6.60E+00, -9.99E+02]],
            dtype=np.float)
        exp = np.transpose(exp)
        self.assertTrue( (exp==self.g.ds).all() )


class TestFlowGridHypso(unittest.TestCase):

    def setUp(self):
        grd_file = os.sep.join(['data','small_grid.grd'])
        dep_file = os.sep.join(['data','small_dep.dep'])
        self.g = FlowGrid(grd_file, dep=dep_file)

    def test_hypso(self):
        d = self.g.hypso()
        # for x in d: print(x)


class TestFlowGridEnc(unittest.TestCase):

    def test_parse_enc_sets_polygon_nm_coordinates(self):
        g = FlowGrid(None)
        enc = textwrap.dedent(
            """
             e=
                 1     1   *** begin left orientated grid enclosure    1
                 7     1
                 7     4
                 8     4
                 8     6
                 7     6
                 7     7
                 3     7
                 3     6
                 2     6
                 2     5
                 1     5
                 1     1   *** end left orientated grid enclosure   1
             e=
                 4     3   *** begin right orientated grid enclosure   1
                 5     3
                 5     4
                 4     4
                 4     3   *** end right orientated grid enclosure   1
            """
        )
        g._parse_enc(enc)
        exp = [
            [(1, 1), (7, 1), (7, 4), (8, 4), (8, 6), (7, 6), (7, 7), (3, 7), (3, 6), (2, 6), (2, 5), (1, 5), (1, 1)],
            [(4, 3), (5, 3), (5, 4), (4, 4), (4, 3)],
        ]
        self.assertSequenceEqual(g.enc_pols, exp)

    def test_parse_enc_with_commas_and_brackets_sets_polygon_nm_coordinates(self):
        g = FlowGrid(None)
        enc = textwrap.dedent(
            """
             e=( 1 ,   1 )
               ( 7 ,   1 )
               ( 7 ,   4 )
               ( 8 ,   4 )
               ( 8 ,   6 )
               ( 7 ,   6 )
               ( 7 ,   7 )
               ( 3 ,   7 )
               ( 3 ,   6 )
               ( 2 ,   6 )
               ( 2 ,   5 )
               ( 1 ,   5 )
               ( 1 ,   1 )
             e=( 4 ,   3 )
               ( 5 ,   3 )
               ( 5 ,   4 )
               ( 4 ,   4 )
               ( 4 ,   3 )
            """
        )
        g._parse_enc(enc)
        exp = [
            [(1, 1), (7, 1), (7, 4), (8, 4), (8, 6), (7, 6), (7, 7), (3, 7), (3, 6), (2, 6), (2, 5), (1, 5), (1, 1)],
            [(4, 3), (5, 3), (5, 4), (4, 4), (4, 3)],
        ]
        self.assertSequenceEqual(g.enc_pols, exp)

    def test_parse_enc_with_commas_and_brackets_old_waqua_format(self):
        g = FlowGrid(None)
        enc = textwrap.dedent(
            """
            E: COORdinates=
               ( 1 ,   1 ) ( 7 ,   1 ) ( 7 ,   4 ) ( 8 ,   4 )
               ( 8 ,   6 ) ( 7 ,   6 ) ( 7 ,   7 ) ( 3 ,   7 )
               ( 3 ,   6 ) ( 2 ,   6 ) ( 2 ,   5 ) ( 1 ,   5 )
               ( 1 ,   1 )
            E: COORdinates=

               ( 4 ,   3 ) ( 5 ,   3 ) ( 5 ,   4 ) ( 4 ,   4 ) ( 4 ,   3 )
            """
        )
        g._parse_enc(enc)
        exp = [
            [(1, 1), (7, 1), (7, 4), (8, 4), (8, 6), (7, 6), (7, 7), (3, 7), (3, 6), (2, 6), (2, 5), (1, 5), (1, 1)],
            [(4, 3), (5, 3), (5, 4), (4, 4), (4, 3)],
        ]
        self.assertSequenceEqual(g.enc_pols, exp)

    def test_enc2cc(self):
        g = FlowGrid(None)
        g.mmax = 4
        g.nmax = 3
        g.xs = np.array(
            [[100., 100., 100.],
             [200., 200., 200.],
             [300., 300., 300.],
             [400., 400., 400.]],
            dtype=np.float)
        g.ys = np.array(
            [[200., 300., 400.],
             [200., 300., 400.],
             [200., 300., 400.],
             [200., 300., 400.]],
            dtype=np.float)
        g.enc_pols = [ [(1, 1),(5, 1),(5, 4),(1, 4),(1, 1)] ]
        exp = [[(100.0, 200.0), (200.0, 200.0), (300.0, 200.0), (400.0, 200.0), (400.0, 300.0), (400.0, 400.0),
                (300.0, 400.0), (200.0, 400.0), (100.0, 400.0), (100.0, 300.0), (100.0, 200.0)]]
        self.assertSequenceEqual(g.enc2cc(), exp)

    def test_polygon_is_clockwise_ccw(self):
        pol = [ (1,1), (5,1), (5,5), (1,5), (1,1) ]
        self.assertFalse(FlowGrid._polygon_is_clockwise(pol))

    def test_polygon_is_clockwise_cw(self):
        pol = [ (1,1), (1,5), (5,5), (5,1), (1,1) ]
        self.assertTrue(FlowGrid._polygon_is_clockwise(pol))

    def test_enc2ind_nw_lu(self):
        """
        o
        |
        o-<-o
        """
        prv, cur, nxt = (6,5), (5,5), (5,6)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,4))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,5))

    def test_enc2ind_nw_ul(self):
        """
        o-<-o
            |
            o
        """
        prv, cur, nxt = (5,4), (5,5), (4,5)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,4))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,5))

    def test_enc2ind_ne_ru(self):
        """
            o
            |
        o->-o
        """
        prv, cur, nxt = (4,5), (5,5), (5,6)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,5))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,4))

    def test_enc2ind_ne_ur(self):
        """
        o->-o
        |
        o
        """
        prv, cur, nxt = (5,4), (5,5), (6,5)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,5))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,4))

    def test_enc2ind_se_rd(self):
        """
        o->-o
            |
            o
        """
        prv, cur, nxt = (4,5), (5,5), (5,4)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,5))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,4))

    def test_enc2ind_se_dr(self):
        """
        o
        |
        o->-o
        """
        prv, cur, nxt = (5,6), (5,5), (6,5)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,5))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,4))

    def test_enc2ind_sw_ld(self):
        """
        o-<-o
        |
        o
        """
        prv, cur, nxt = (6,5), (5,5), (5,4)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,4))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,5))

    def test_enc2ind_sw_dl(self):
        """
            o
            |
        o-<-o
        """
        prv, cur, nxt = (5,6), (5,5), (4,5)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,4))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,5))

    def test_enc2ind_nn(self):
        """
            to
            o
            |
            o
            |
            o
          from
        """
        prv, cur, nxt = (5,4), (5,5), (5,6)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,4))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,4))

    def test_enc2ind_ss(self):
        """
          from
            o
            |
            o
            |
            o
           to
        """
        prv, cur, nxt = (5,6), (5,5), (5,4)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,5))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,5))

    def test_enc2ind_ee(self):
        """
        from o---o---o to
        """
        prv, cur, nxt = (4,5), (5,5), (6,5)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,5))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (4,4))

    def test_enc2ind_ww(self):
        """
        to o---o---o from
        """
        prv, cur, nxt = (6,5), (5,5), (4,5)
        left = True
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,4))
        left = False
        ind = FlowGrid._enc2ind(prv, cur, nxt, left)
        self.assertEqual(ind, (5,5))


    def test_expand_diagonals_ne(self):
        pol = [ (1,1), (4,1),        (5,2), (5,5), (1,5), (1,1) ]
        left = True
        exp = [ (1,1), (4,1), (5,1), (5,2), (5,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)
        left = False
        exp = [ (1,1), (4,1), (4,2), (5,2), (5,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)

    def test_expand_diagonals_se(self):
        pol = [ (5,5), (1,5), (1,2),        (2,1), (5,1), (5,5) ]
        left = True
        exp = [ (5,5), (1,5), (1,2), (1,1), (2,1), (5,1), (5,5) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)
        left = False
        exp = [ (5,5), (1,5), (1,2), (2,2), (2,1), (5,1), (5,5) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)

    def test_expand_diagonals_sw(self):
        pol = [ (1,1), (5,1), (5,5), (2,5),        (1,4), (1,1) ]
        left = True
        exp = [ (1,1), (5,1), (5,5), (2,5), (1,5), (1,4), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)
        left = False
        exp = [ (1,1), (5,1), (5,5), (2,5), (2,4), (1,4), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)

    def test_expand_diagonals_nw(self):
        pol = [ (1,1), (5,1), (5,4),        (4,5), (1,5), (1,1) ]
        left = True
        exp = [ (1,1), (5,1), (5,4), (5,5), (4,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)
        left = False
        exp = [ (1,1), (5,1), (5,4), (4,4), (4,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)

    def test_expand_diagonals_ne2(self):
        pol = [ (1,1), (3,1),                      (5,3), (5,5), (1,5), (1,1) ]
        left = True
        exp = [ (1,1), (3,1), (4,1), (4,2), (5,2), (5,3), (5,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)
        left = False
        exp = [ (1,1), (3,1), (3,2), (4,2), (4,3), (5,3), (5,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)

    def test_expand_diagonals_se2(self):
        pol = [ (5,5), (1,5), (1,3),                      (3,1), (5,1), (5,5) ]
        left = True
        exp = [ (5,5), (1,5), (1,3), (1,2), (2,2), (2,1), (3,1), (5,1), (5,5) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)
        left = False
        exp = [ (5,5), (1,5), (1,3), (2,3), (2,2), (3,2), (3,1), (5,1), (5,5) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)

    def test_expand_diagonals_sw2(self):
        pol = [ (1,1), (5,1), (5,5), (3,5),                      (1,3), (1,1) ]
        left = True
        exp = [ (1,1), (5,1), (5,5), (3,5), (2,5), (2,4), (1,4), (1,3), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)
        left = False
        exp = [ (1,1), (5,1), (5,5), (3,5), (3,4), (2,4), (2,3), (1,3), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)

    def test_expand_diagonals_nw2(self):
        pol = [ (1,1), (5,1), (5,3),                      (3,5), (1,5), (1,1) ]
        left = True
        exp = [ (1,1), (5,1), (5,3), (5,4), (4,4), (4,5), (3,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)
        left = False
        exp = [ (1,1), (5,1), (5,3), (4,3), (4,4), (3,4), (3,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)

    def test_expand_diagonals_ne3(self):
        pol = [ (1,1), (2,1),                                    (5,4), (5,5), (1,5), (1,1) ]
        left = True
        exp = [ (1,1), (2,1), (3,1), (3,2), (4,2), (4,3), (5,3), (5,4), (5,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)
        left = False
        exp = [ (1,1), (2,1), (2,2), (3,2), (3,3), (4,3), (4,4), (5,4), (5,5), (1,5), (1,1) ]
        self.assertSequenceEqual(FlowGrid._expand_diagonals(pol, left), exp)

    def test_expand_indices_raises_exception_for_diagonals(self):
        ind = [(949,286),(949,287),(949,287),(950,288),(952,287),(949,286)]
        with self.assertRaises(ValueError):
            FlowGrid._expand_indices(ind)


def _run_unittests():
    unittest.main()


if __name__ == '__main__':
    _run_unittests()
