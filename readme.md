# d3d.py#

Read/write access to [Delft-3D](http://oss.deltares.nl/web/delft3d) files in python.

Work in progress, supported files so far:

* WAQ binary output: \*.his, \*.map
* WAQ static segment-based com-files: \*.srf, \*.dps
* WAQ static exchange-based com-files: \*.len, \*.poi
* WAQ dynamic segment-based com-files: \*.vol
* WAQ dynamic exchange-based com-files: \*.are, \*.flo
* WAQ hyd-file: \*.hyd
* (Flow grid: \*.grd)
* (Flow depth: \*.dep)

## Dependencies ##
Only [NumPy](http://www.numpy.org) is required.
See [here](http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy) for Windows installers.

## Quikstart ##
### Access modes ###
Files open in read mode by default:
```
#!python
>>> f = d3d.DelwaqPoiFile('example.poi')
```

Use mode 'r+' to modify an existing file.
```
#!python
>>> f = d3d.DelwaqPoiFile('example.poi', mode='r+')
```

and 'w' to create a new file. Some file dimensions may be required, depending on the file format.
```
#!python
>>> f = d3d.DelwaqPoiFile('example.poi', mode='w', noq=100)
```

### Reading and Writing ###
Read data as if files were arrays.
```
#!python
>>> f = d3d.DelwaqPoiFile('example.poi')
>>> data = f[:,:]
```

Write data the same way.
```
#!python
>>> f = d3d.DelwaqPoiFile('example.poi', mode='r+')
>>> f[15:100,:] = data
```
Print a file object to know about it's data dimensions (or check the source).

Most binary files provide '''dump()''' to export data to ascii.

### ASCII files ###
Access data through attributes.
```
#!python
>>> hyd = d3d.DelwaqHydFile('example.hyd')
>>> hyd.zlayers
True
```

Write data by setting attributes and calling
```save()``` or ```save_as(*target*)```.
```
#!python
>>> hyd = d3d.DelwaqHydFile('example.hyd', mode='r+')
>>> hyd.zlayers = False
>>> hyd.save()
```