#!/usr/bin/env python3
from distutils.core import setup

setup(name='d3d.py',
      version='0.1',
      description='Delft3D toolbox',
      author='Christophe Thiange',
      author_email='Christophe.Thiange@deltares.nl',
      url='https://bitbucket.org/cthiange/d3d',
	  py_modules = ['d3d']
     )